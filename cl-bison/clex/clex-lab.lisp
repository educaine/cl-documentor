(in-package :cl-bison)


;; primitive representation of a transition diagrame for matching 1*2\.3:

(defparameter *state-machine-desc*
  '((:state-name 1 :transitions ((:match #\2 :to-state 2)
				  (:match #\1 :to-state 1)))
    (:state-name 2 :transitions ((:match #\. :to-state 3)))
    (:state-name #\3 :transitions nil)))





(defun generate-state-machine-transition (state)
  (let ((switch-clauses
	 (destructuring-bind (&key  state-name transitions &allow-other-keys)
	     state
	   (mapcar
	    (lambda (transition)
	      (setf *result*
		    (destructuring-bind (&key  match to-state &allow-other-keys)
			transition
		      `(,match (setf *current-state* ,to-state)))))
	    transitions))))
    `(lambda (token)
      (switch token
       ,switch-clauses))))



    (defun test (&key state-name transitions)
      (list state-name transitions))




        (destructuring-bind (&key  state-name transitions  &allow-other-keys)
	'(:state-name 1 :transitions 1))

	(car *state-machine-desc*) (list transitions))
