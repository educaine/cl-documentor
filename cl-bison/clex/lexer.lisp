(in-package :cl-bison)

(defclass lexer ()
  ((filename :initarg :filename :initform nil)
   (peek :initarg :peek)
   (reserved-words :accessor reserved-words)
   (lexer-char-byte-stream :accessor lexer-char-byte-stream)))


;; (defparameter *char-byte-stream
;;   (open "~/dev-local/common-lisp/cl-documentor/cl-bison/clex/test.java"
;; 	     :direction :output
;; 	     :element-type 'unsigned-byte))
(defgeneric open-file-for-lexing (lexer filename))

(defmethod open-file-for-lexing ((l lexer) filename)
  (setf (lexer-char-byte-stream l)
	(open "clex/test.java"
	      :direction :input
	      :element-type 'unsigned-byte)))

(defparameter *lexer* (make-instance 'lexer))

(defun run-open-file-for-lexing ()
  (unless *lexer*
    (setf *lexer* (make-instance 'lexer)))
  (open-file-for-lexing
   *lexer*
   "clex/test.java"))

(defgeneric lexer-scan (lexer))


(defun lex-reader (stream a b)
  (read-byte stream nil nil))

(defmethod lexer-scan ((l lexer))
  (with-slots (filename peek reserved-words lexer-char-byte-stream) l
    (let ((chars
	   (iterate
	     (for char-byte in-stream lexer-char-byte-stream using #'read-byte)
	     (collect char-byte))))
      (iterate (for char in chars)
	       (cond
		 (



(defun run-lexer-scan ()
  (run-open-file-for-lexing)
  (lexer-scan *lexer*))
