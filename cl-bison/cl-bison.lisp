;; The type of closing bracket we want
(in-package :cl-bison)

(defparameter bstack nil)

;; 'bracket' stack pointer
(defparameter bsp 0)

(defun next-lex ()
  (funcall *lex*))

(defun i2p (a b c)
  (list b a c))

(defun i1p (a b)
  (list b a))

(defparameter *expression-parser* nil)

(defparameter *lex* nil)

(defun run-deflexer ()
  (deflexer top-level-lexer
    ("[0-9]+([.][0-9]+([Ee][0-9]+)?)"
     (return (values 'flt (num %0))))
  ("[0-9]+"
   (return (values 'int (int %0))))
  ("[([{<'\"`]"
   (return (values 'left-bracket %0)))
  ("[)}>'\"\\]]"
   (return (values 'right-bracket %0)))
  ("\\+"
   (return (values '+ %0)))
  ;; @blankspace
  ("@[bB][lL][aA][nN][kK][sS][pP][aA][cC][eE]\\w*\\["
   (setf *lex-state*
   (return (values '(blankspace left-bracket) '("@blankspace" "[")))))))

  ;; ("[:alpha:][:alnum:]*"
  ;;  (return (values 'name %0)))
  ;; ("[ \t]+")))))

;; @[bB][lL][aA][nN][kK][sS][pP][aA][cC][eE]/{LB} {
;;                       BEGIN BS;
;;                       if ( lastsym=lookup(yytext) )
;;                              return (lastsym->s_type);
;;                       fprintf(stderr,"lex botch: keyword %s\n", yytext );
;;                       exit(1);
;;                       }


(defparameter *lex-state* nil)
(defparameter *top-level-lexer* nil)

(defun run-setf-lexer ()
  (let ((buffer-tokens '())
	(buffer-values '()))
    (setf *top-level-lexer* (top-level-lexer "@blankspace[1 + 1]"))
    (setf *lex-state* *top-level-lexer*)
    (setf *lex* (lambda ()
		  (if buffer-tokens
		      (values (pop buffer-tokens) (pop buffer-values))
		      (multiple-value-bind (token value)
			  (funcall *lex-state*)
			(cond
			  ((atom token)
			   (values token value))
			  (t
			   (push (cadr token) buffer-tokens)
			   (push (cadr value) buffer-values)
			   (values (car token) (cdr token))))))))))


;; (defun run-setf-lexer ()
;;   (setf *top-level-lexer* (top-level-lexer "@blankspace[1 + 1]"))
;;   (setf *lex* (lambda ()
;; 		(case *lex-state*
;; 		  (

;; WS      [ \t]*
;; LB      ("("|"["|"{"|"<"|\'|\"|\`)
;; RB      (")"|"]"|"}"|">"|\'|\"|\')

;; CS: command section?
;; BS: blankspace
;;  B: inside blankspace bracket
;;  C: inside command section bracket


(defun k-2-3 (a b c)
  (declare (ignore a c))
  b)

(defun run-define-parser ()
    `(define-parser *expression-parser*
      (:start-symbol expression)
      (:terminals (int id flt name + - * / |(| |)|
		       left-bracket
		       right-bracket))
      (:precedence ((:left * /) (:left + -)))
      (expression
       (expression + expression #'i2p)
       (expression - expression #'i2p)
       (expression * expression #'i2p)
       (expression / expression #'i2p)
       term)
      (term
       flt
       id
       int
       (- term)
       (left-bracket expression right-bracket)
       (|(| expression |)| #'k-2-3))))

(defun run-parser ()
  (parse-with-lexer
    *lex*
    *expression-parser*))

(defun run-function ()
  (eval (run-define-parser))
  (run-deflexer)
  (run-setf-lexer))

;; @[bB][eE][gG][iI][nN]{WS}{LB} return(get_env_name());
;; @[eE][nN][dD]{WS}{LB}   {
;;                       get_env_name();
;;                       return(END_ENV);
;;                       }
;; @[a-zA-Z0-9]+/{WS}{LB}  {
;;                       BEGIN CS;
;;                       if ( !(lastsym=lookup(yytext)) )
;;                              {
;;                              if ( kflag )
;;                                     fprintf(stderr,"unknown keyword %s\n",
;;                                            yytext );
;;                              lastsym = enter( yytext, KW_REP, &yytext[1] );
;;                              }
;;                       return (lastsym->s_type);
;;                       }
;; @[a-zA-Z0-9]+          return(COMMAND);
;; @\+/{WS}{LB}           {
;;                       BEGIN CS;
;;                       return(SUP);
;;                       }
;; @\-/{WS}{LB}           {
;;                       BEGIN CS;
;;                       return(SUB);
;;                       }
;; <CS>{WS}               ;
;; <CS>{LB}               {
;;                       bstack[++bsp] = matching(*yytext);
;;                       BEGIN C;
;;                       return(LBRACK);
;;                       }
;; <BS>{WS}               ;
;; <BS>{LB}               {
;;                       bstack[++bsp] = matching(*yytext);
;;                       BEGIN B;
;;                       return(LBRACK);
;;                       }
;; <B,C>{RB}              {
;;                       if ( bstack[bsp] == *yytext ) {
;;                              if ( --bsp <= 0 )
;;                                     BEGIN 0;
;;                              return(RBRACK);
;;                              }
;;                       return( (*yytext=='}'||*yytext=='"')? *yytext : CHAR );
;;                       }
;; <B>[iI][nN][cC][hH][eE][sS]     { return(INCHES); }
;; @\\                   return(TAB);
;; @>                    return(RJUST);
;; @=                    return(CENTER);
;; @\\{WS}@=              return(CENTER);
;; @"^"                  return(SETTAB);
;; @\\{WS}@=/{WS}\n        ;
;; @=/{WS}\n              ;
;; @\\/{WS}\n             ;
;; @;                    ;
;; @~                    return(STILDE);
;; @_                    return(HYPHEN);
;; @" "                  return(NPSPACE);
;; @@                    return('@');
;; @"*"                  return(LBREAK);
;; @\.[ \t]+              return(POINT);
;; @\.{WS}/\n             return(POINT);
;; [<>|{}#$%&~_^\\"]       return(*yytext);
;; .                     return(CHAR);
;; ^[      ]*\n          {
;;                       inputline++;
;;                       return(BLANKLINE);
;;                       }
;; [       ]*\n          {
;;                       inputline++;
;;                       return('\n');
;;                       }
;; %%
;; yywrap() {
;;         return (1);
;; }

;; /* get the "name" part of an environment spec & discard any optional
;;  * parameters.  The name is added to the symbol table if it isn't
;;  * already there.  At the time we call this routine, we've matched
;;  * either "@begin{LB}" or "@end{LB}".  We gobble input until we find
;;  * the matching right bracket.
;;  */
;; get_env_name()
;; {
;;         char   ename[128];
;;         char   c;
;;         char   *nm=ename;
;;         char   mb = matching( yytext[yyleng-1] );

;;         /* get the name */
;;         while ( isalnum( c=input() ) )
;;                *nm++ = c;
;;         *nm = '\0';

;;         /* discard everything else up to the closing bracket */
;;         while ( c != mb )
;;                c = input();

;;         /* lookup and/or add the env name */
;;         if ( ! (lastsym = lookup(ename)) )
;;                {
;;                if ( kflag )
;;                       fprintf(stderr,"unknown environment %s\n", ename );
;;                lastsym = enter( ename, ENV_REP, ename );
;;                }
;;         return(lastsym->s_type);
;; }

;; /* return the right bracket character that matches the given left bracket
;;  * character.
;;  */
;; char matching(lb)
;; char lb;
;; {
;;         switch(lb) {

;;         case '(':  return(')');
;;         case '[':  return(']');
;;         case '{':  return('}');
;;         case '<':  return('>');
;;         case '"':  return('"');
;;         case '\'':  return('\'');
;;         case '`':  return('\'');
;;         default:
;;                fprintf(stderr,"matching botch: '%c'\n", lb );
;;                exit(1);
;;         }
;; }




;; (defun process-m4 (input-stream output-stream)
;;   (let* ((*m4-quote-start* "`")
;;          (*m4-quote-end* "'")
;;          (*m4-comment-start* "#")
;;          (*m4-comment-end* "\\n")
;;          (*m4-macro-name* "[_a-zA-Z]\\w*")
;;          (lexer (make-instance 'lexer-input-stream
;;                                :stream input-stream
;;                                :rules '((*m4-comment-start* . :comment-start)
;;                                         (*m4-comment-end* . :comment-end)
;;                                         (*m4-macro-name* . :macro-name)
;;                                         (*m4-quote-start* . :quote-start)
;;                                         (*m4-quote-end* . :quote-end)
;;                                         ("," . :comma)
;;                                         ("\\n" . :newline)
;;                                         (" " . :space)
;;                                         ("\\(" . :open-paren)
;;                                         ("\\)" . :close-paren)
;;                                         ("." . :token)))))
;;     (dump-m4-tokens lexer)))

;; (defpackage :graylex-system
;;   (:use :cl))

;; (defun dump-m4-tokens (lexer)
;;   (do* ((token (multiple-value-list (graylex::stream-read-token lexer))
;;                (multiple-value-list (graylex::stream-read-token lexer)))
;;         (class (car token) (car token))
;;         (image (cadr token) (cadr token)))
;;        ((null class))
;;     (format t "~a:~a " class
;;             (cond ((equal :quote-start class)
;;                    (prog1 image ; normally you'd call the next production rule function here
;;                      (setq *m4-quote-start* "\\[")))
;;                   ((equal :quote-end class)
;;                    (prog1 image
;;                      (setq *m4-quote-end* "\\]")))
;;                   ((equal :comment-start class)
;;                    (prog1 image
;;                      (setq *m4-comment-start* "/\\*")))
;;                   ((equal :comment-end class)
;;                    (prog1 image
;;                      (setq *m4-comment-end* "\\*/")))
;;                   ((equal :macro-name class)
;;                    (prog1 image
;;                      (when (string= "foo" image)
;;                        (setq *m4-macro-name* "\\d+"))))
;;                   (t image)))))

;; (defun run-make-lexer ()
;;   (let* ((input-stream (make-string-input-stream "1 + 1"))
;; 	 (*m4-quote-start* "`")
;; 	 (*m4-quote-end* "'")
;; 	 (*m4-comment-start* "#")
;; 	 (*m4-comment-end* "\\n")
;; 	 (*m4-macro-name* "[_a-zA-Z]\\w*")
;; 	 (lexer (make-instance 'graylex::lexer-input-stream
;; 			       :stream input-stream
;; 			       :rules '((*m4-comment-start* . :comment-start)
;; 					(*m4-comment-end* . :comment-end)
;; 					(*m4-macro-name* . :macro-name)
;; 					(*m4-quote-start* . :quote-start)
;; 					(*m4-quote-end* . :quote-end)
;; 					("," . :comma)
;; 					("\\n" . :newline)
;; 					(" " . :space)
;; 					("\\(" . :open-paren)
;; 					("\\)" . :close-paren)
;; 					("." . :token)))))
;; ;;;    lexer))
;;     (dump-m4-tokens lexer)))


;;(use-package :dso-lex)
;;(use-package :cl-ppcre :dso-lex)





;; (defun single-tic (s)
;;   (format t "single tic section ~s~%" s))

;; (defun double-tic (s)
;;   (format t "double tic section ~s~%" s))

;; (deflexer scan-csv (:priority-only t)
;;   ("," comma)
;;   ("[^\"',]+" value)
;;   ("'(?:[^']|'')*'" value single-tic)
;;   ("\"(?:[^\"]|\"\")*\"" value double-tic))

;; (defun run-lexer ()
;;   (lex-all 'scan-csv "no quotes,'a ''quote''',\"another \"\"quote\"\"\""))

;; (defun maybe-unread (char stream)
;;   (when char
;;     (unread-char char stream)))

;; (defparameter string-output-stream (make-string-input-stream "1 + 1"))


;; (defun make-multiple-value-bind ()
;;   (multiple-value-bind (type value) (lexer) (list (type-of type) (Type-of value))))

;; (defun read-number (stream)
;;   (let ((v nil))
;;     (loop
;;        (let ((c (read-char stream nil nil)))
;;          (when (or (null c) (not (digit-char-p c)))
;;            (maybe-unread c stream)
;;            (when (null v)
;;              (lexer-error c))
;;            (return-from read-number v))
;;          (setf v (+ (* (or v 0) 10) (- (char-code c) (char-code #\0))))))))

;; (defun lexer (&optional (stream *standard-input*))
;;   (loop
;;      (let ((c (read-char stream nil nil)))
;;        (cond
;;          ((member c '(#\Space #\Tab)))
;;          ((member c '(nil #\Newline)) (return-from lexer (values nil nil)))
;;          ((member c '(#\+ #\- #\* #\/ #\( #\)))
;;           (let ((symbol (intern (string c) '#.*package*)))
;;             (return-from lexer (values symbol symbol))))
;;          ((digit-char-p c)
;;           (unread-char c stream)
;;           (return-from lexer (values 'int (read-number stream))))
;;          ((alpha-char-p c)
;;           (unread-char c stream)
;;           (return-from lexer (values 'id (read-id stream))))
;;          (t
;;           (lexer-error c))))))

;; ;; (defun run-lexer ()
;; ;;   (defparameter input-stream (make-string-input-stream "2 + (1 + 1)")))
;; ;; (deflexer scan-csv2 (:priority-only t)
;; ;;   ("[ \t]+" ws)
;; ;;   ("," comma)
;; ;;   ("[^\"',]+" value)
;; ;;   ("'(?:[^']|'')*'" value single-tic)
;; ;;   ("\"(?:[^\"]|\"\")*\"" value double-tic))

;; ;; (lex-all 'scan-csv2
;; ;; 	 (concatenate 'string
;; ;; 		      "no quotes,'a ''quote''',"
;; ;; ;;		      (string #\Tab)
;; ;; 		      "\"another \"\"quote\"\"\""))

;; (defun run-define-parser2 ()
;;   (define-parser *expression-parser2*
;;     (:start-symbol expression)
;;     (:terminals (int id + - * / |(| |)|))
;;     (:precedence ((:left * /) (:left + -)))
;;     (expression
;;      (expression + expression #'i2p)
;;      (expression - expression #'i2p)
;;      (expression * expression #'i2p)
;;      (expression / expression #'i2p)
;;      term)
;;     (term
;;      id
;;      int
;;      (- term)
;;      (|(| expression |)| #'k-2-3))))


;; (defun list-lexer (list)
;;   #'(lambda ()
;;       (let ((value (pop list)))
;; 	(if (null value)
;; 	    (values nil nil)
;; 	    (let ((terminal
;; 		   (cond ((member value '(+ - * / |(| |)|)) value)
;; 			 ((integerp value) 'int)
;; 			 ((symbolp value) 'id)
;; 			 (t (error "Unexpected value ~S" value)))))
;; 	      (values terminal value))))))

;; (defun run-parser ()
;;   (parse-with-lexer
;;    (list-lexer '(x * - - 2 + 3 * y))
;;    *expression-parser*))





;; WS      [ \t]*
;; LB      ("("|"["|"{"|"<"|\'|\"|\`)
;; RB      (")"|"]"|"}"|">"|\'|\"|\')

;; CS: command section?
;; BS: blankspace
;;  B: inside blankspace bracket
;;  C: inside command section bracket

;; inclusive every patern except those match the following are active.
;; %START  CS BS B C

;; %%
