

cat > flex-test.l << FLEX_TEST_L
%{
/* need this for the call to getlogin() below */
#include <unistd.h>
%}

%%
[\]]	        printf("Yasss, got a ']'.\n");
%%

int main() {
  yylex();
}

FLEX_TEST_L


flex flex-test.l
gcc lex.yy.c -lfl
echo "]" | ./a.out
