;;; -*- Syntax: Zetalisp; Package: LEXER; Base: 10; Mode: LISP -*-

(defparameter  b (make-array '(2 3)
			     :displaced-to a
			     :displaced-index-offset (* 1 2 3)))

(defparameter a
  (make-array '(4 2 3) :initial-contents '(((a b c) (1 2 3))
					   ((d e f) (3 1 2))
					   ((g h i) (2 3 1))
					   ((j k l) (0 0 0)))))

(defun array-dimension-n (index array)
  (let* ((rest-dimensions (cdr (array-dimensions array)))
	 (offset (apply #'* (cons index rest-dimensions))))
    (make-array rest-dimensions
		:displaced-to array
		:displaced-index-offset offset)))

;;; This function is a simplified version of the lexer, the intent
;;; here is to determine whether a leading match has been made, and
;;; the length of the match. The simple automaton passed in is a list
;;; consisting of the automaton table, followed by the states that
;;; constitute the goal state for this automaton.
;;;
;;; next-char is a function that yields the next character each time
;;; it is invoked.  A nil signals that no more characters are
;;; available.  The function returns a boolean
(defun match-compiled-regex (next-char simple-automaton)
  (declare (values match-p eof-p length))
  (loop with automaton = (car simple-automaton)
     with rows = (array-dimension-n 1 automaton) and cols = (array-dimension-n 2 automaton)
     and goal and current-state = 0 and last-match-length = 0
     for char = (funcall next-char)
     for token-length from 0
     while (and char
		(setq current-state
		      (and (< char cols)
			   (let ((state (aref automaton current-state char)))
			     (when (memq state (cdr simple-automaton))
			       (setq goal state
				     last-match-length (+ token-length 1)))
			     (when (< state rows)
			       state)))))
     finally (return (values goal (null char) last-match-length))))
