;;; -*- Syntax: Zetalisp; Package: LEXER; Base: 10; Mode: LISP -*-

(defconstant lexer-wraparound-char #\0)

;;;
;;; The variable that is bound to the lexer currently being used.
;;;
(defvar *lexer* nil)

;;;
;;; 		LEXER definition
;;;

(defflavor lexer
	((lexer-sets nil)
	 (time (time:print-current-time nil))	; The time that the lexer was built
	 (skip-token nil)			; True if the current token will be
						; skipped, ie "next-token" will not return
	 start-state				;
	 State-to-regex				; An array indexed by state that maps
						; to a set of regular expressions that
						; that are recognized in that state
	 (stream nil)				; The stream object that is the source
						; of characters.
	 (eof-p nil)				; True iff the stream is at EOF
	 (error-stream t)			; Do we use this ?????????????????? TBD...
	 (buffer-size 512)			; The buffer where characters are accumulated
	 ;; circular buffer for input of characters -- 512 because that's what Lex uses.
	 ;; The last character in the buffer is the lexer-wraparound-char if the buffer
	 ;; has not wrapped around for the stream.
	 (buffer (cl:make-array 512. :element-type 'character))
	 (start 0)		; one less than the index to the first character in the
	 (cursor 0)		; points to the last character that has been seen by
				; the lexer
	 (end-of-lookahead 0)	; indicates the end of the text from a previous lookahead
				; It actually points one past the last lookahead character
	 (last-match 0)		; index into buffer for last character which was a match
	 (illegal-lexeme #'default-illegal-lexeme) ; Handler for an illegal lexeme
	 (pseudo-terminals nil)
	 )
	(grammar::comb)
  :initable-instance-variables
  :writable-instance-variables
  :readable-instance-variables)

(defmethod (copy-lexer lexer)
	   ()
  (make-instance 'lexer
		 :time			(lexer-time self)
		 :skip-token		(lexer-skip-token self)
		 :start-state		(lexer-start-state self)
		 :State-to-regex	nil  ;; State-to-regex is what will be replaced
		 :error-stream		(lexer-error-stream self)
		 :buffer-size		(lexer-buffer-size self)
		 ;;			Each separate lexer needs its own internal buffer
		 :buffer		(cl:make-array (lexer-buffer-size self)
						       :element-type 'character)
		 :illegal-lexeme	(lexer-illegal-lexeme self)
		 ;; 			comb is a readonly array
		 :comb 			(grammar::comb-comb self)
		 ;;			 valid-entry-matrix is a readonly array
		 :valid-entry-matrix	(grammar::comb-valid-entry-matrix self)
		 ;;			 comb-index-array is a readonly array
		 :comb-index-array	(grammar::comb-comb-index-array self)
		 :pseudo-terminals	(lexer-pseudo-terminals self)))

(defmethod (handles-derivers-p lexer) () nil)
(defmethod (get-derivers lexer) () nil)
(defmethod (set-derivers lexer) (derivers-list)
  (ferror "A lexer which does not handle derivers was passed a derivers list: ~%~A"
	  derivers-list))

;;;
;;; The following functions define the protocol to the lexer. They appear in the order that
;;; they would normally be invoked during a run of the lexer.
;;;

;;; Reset the lexer. This method resets the lexer state. It must be invoked before
;;; the first call to next-token. Subsequently it may be used to discard "lookahead"
;;; characters, for example when the stream object has been changed. Or when the stream
;;; has been advanced without the knowledge of the lexer (eg. in skipping comments)
;;;
;;; effects
;;;   1) Reinitializes the lexer
;;;   2) Flushes its input buffer, discarding any lookahead characters
;;;      and discards all left context.
;;;   3) Changes the stream if specified
;;;
(defmethod (reset-lexer lexer)(&optional (input-stream stream))
  (setq start 0
	cursor 0
	eof-p  nil
	end-of-lookahead 0
	last-match 0
	stream input-stream)
  ;; The character in the buffer serves as a flag to indicate whether the buffer was
  ;; ever wrapped around.
  (setf (aref buffer (- buffer-size 1)) #\0))

;;; This is the function that is invoked when the lexer is unable to identify a string
;;; as a defined regular expression. The user will probably define his own function
;;; to handle such cases.
(defun default-illegal-lexeme (lexer)
  (ferror "Illegal lexeme. Unrecognized text follows enclosed in double quotes. /"~A/""
	  (match-string lexer)))

(defsubst execute-lexer-actions(regular-expressions lexer)
  (send regular-expressions :execute-action lexer))

(defmacro incf-lexer-cursor(cursor)
  `(progn
    (when ((incf ,cursor) buffer-size) (setf ,cursor 0))
    (when (= ,cursor start) (enlarge-buffer self))
    ,cursor))

(defmacro decf-lexer-cursor (cursor)
  `(progn
    (when (< (decf ,cursor) 0) (setf ,cursor (1- buffer-size)))
    ,cursor))

;;; Get the next input character, it may either be the residue from
;;; the previous lookahead, or may genuinely need to be obtained via a
;;; tyi:
(defmacro next-lexer-char ()
  `(cond
    ((cursor end-of-lookahead)
     (aref buffer (incf-lexer-cursor cursor)))
    (t
     (let ((char (send stream :tyi)))
       (if char
	 (setf (aref buffer (setq end-of-lookahead (incf-lexer-cursor cursor))) char)
	 (not (setq eof-p t)))))))

;;; Initialize the lexer in preperation for recognizing a token.
(defmacro initialize-lexer-pos()
  `(setq eof-p  nil
	 ;; Set the cursor to the last character that was matched as part of a token
	 ;; This permits rescanning any lookahead characters.
	 cursor last-match
	 ;; Start position.
	 start  cursor
	 skip-token nil))

(defmacro next-lexer-state (row col &environment env)
  (once-only (row col &environment env)
    `(unless (zerop (aref grammar::valid-entry-matrix ,row ,col))
	 (aref grammar::comb (+ (aref grammar::comb-index-array ,row) ,col)))))


;;; Use this function when you wish to continue the lexer recognition loop in
;;; a call to next-token after causing a "program state" change in a lexer action.
;;; Specify ":skip" as the "action" to skip a token directly within the lexer.
(defmacro continue-lexer (&optional lexer)
  `(setf (lexer-skip-token (or ,lexer *lexer*)) t))

(defmacro lexer-match-string() `(match-string *lexer*))

;;;
;;; This method may be used to obtain the actual string that was matched by the lexer
;;;
;;; requires
;;;    A match has been made or an illegal token detected
;;;
;;; effects
;;;   returns a pointer(nsubstring) to the string that was accepted/rejected.
;;;
(defmethod (match-string lexer) ()
  (let ((match-start (if ( (1+ start) buffer-size) 0 (1+ start)))
	(match-end  (if eof-p (1+ cursor) cursor)))
    (cond
      (( match-end match-start) (nsubstring buffer match-start match-end))
      (t (string-append (nsubstring buffer match-start buffer-size)
			(nsubstring buffer 0 match-end))))))


;;;
;;; Method to obtain the "n" lines of left context. This method is primarily useful
;;; when producing error messages, and the textual context of the error needs to be
;;; established.
;;;
;;; effects
;;;   returns a pointer(nsubstring) to "lines"
;;;
(defmethod (:current-left-context lexer)(&optional (lines 1))
  (check-arg lines (> lines 0) "must be positive")
  (loop for i below lines
        for j = (string-reverse-search-char #/return buffer cursor)
	      then (string-reverse-search-char #/return buffer j)
	while j
	finally
	  (return
	    (cond
	      (j (nsubstring buffer j  cursor))

	      ((char= (aref buffer (- buffer-size 1)) #\0)
	       ;; No wraparound, simply return the string from the start.
	       (nsubstring buffer 0 cursor))

	      (t (loop for i below (- lines i)
		       for prev-j = j
		       for j = (string-reverse-search-char #/return buffer buffer-size
							   (1+ cursor))
			     then (string-reverse-search-char #/return buffer j cursor)
		       while j
		       finally
			 (setq j (or j prev-j))
			 (return
			   (cond (j (string-append (nsubstring buffer (1+ j) buffer-size)
						   (nsubstring buffer 0 cursor)))
				 (t (nsubstring buffer 0 cursor))))))))))


;;; Return the lookahead character back to the stream. This operation is normally performed
;;; when the lexer is about to have its input switched to a different stream. Input from the
;;; current stream will be resumed at some later point in time.
;;;
;;; requires
;;;    There may be atmost one lookahead character currently in the buffer
;;;
;;; effects
;;;    Untyis the lookahead character to the stream
;;;
(defmethod (:untyi-lookahead lexer)()
  (when (cursor last-match)
    ;; There is text to be untyid
    (let ((untyi-char (aref buffer cursor)))
      (when ((decf-lexer-cursor cursor) last-match)
	(ferror "> 1 lookahead character, cannot untyi"))
      (setq end-of-lookahead cursor)
      ;; :untys if the stream can respond to it ???? TBD...
      ;; Under the kludge model, the unless below prevents untyi of the garbage character
      ;; which corresponds to EOF.
      (unless eof-p (send stream :untyi untyi-char)))))

;;; If you need to inspect the lookahead character (like the Symbolics
;;; C compiler does) you can use this method.
(defmethod (get-lookahead-char lexer) ()
  (when (cursor last-match)
    (aref buffer cursor)))

;;; This code should not be called if you are not certain that
;;; decrementing the cursor will not drop you out of the buffer.  It
;;; is used currently by the editor where the line IS the buffer and
;;; we know there is no wraparound (i.e. - no circular buffer).
(defun undo-multiple-char-lookahead (&optional (num-chars 1) (lexer *lexer*))
  (zl:check-arg num-chars (and (fixnump num-chars) (> num-chars 0)) "a fixnum > 0")
  (setq lexer (eval lexer))
  (send lexer :untyi-lookahead)
  (setf (lexer-cursor lexer) (- (lexer-cursor lexer) num-chars))
  (setf (lexer-last-match lexer) (lexer-cursor lexer)))

(defmethod (enlarge-buffer lexer) ()
  ;; This is the place for a proceed option offering to grow the array.
  (if (cl:cerror "Increase the input buffer's size" "Overran input buffer")
      (ferror "Huh?")
      (let ((old-buffer-size buffer-size))
	(incf buffer-size 512)
	(cl:adjust-array buffer buffer-size)
	(cond
	  ((zerop cursor)
	   (setf cursor old-buffer-size))
	  ((> start cursor)
	   (loop with end-index = (1- start)
		 for i upfrom 0
		 for cursor upfrom (1+ old-buffer-size)
		 until (= i end-index)
		 do (setf (aref buffer cursor) (aref buffer i))))))))

(defmethod (next-token lexer) ()
  (loop
   as token =
      (loop initially
	      (initialize-lexer-pos)
	      (setq current-state start-state)
	    with match-exp
	    for char = (next-lexer-char)
	    while char
	    for current-state = (next-lexer-state current-state (char-code char))
	    while current-state
	    for reg-exp = (aref State-to-regex current-state)
	    when reg-exp
	      do (setq last-match cursor
		       match-exp  reg-exp)
	    finally
	      (return
		(cond
		  (match-exp
		   ;; Back up the cursor for handling match-string properly.
		   (when char
		     (setq cursor
			   (if ( (1+ last-match) buffer-size) 0 (1+ last-match))))
		   ;; Made a match. Execute the associated actions.
		   (execute-lexer-actions match-exp self))
		  ((cursor start)
		   ;; No match, invoke the illegal lexeme processor
		   (setq last-match cursor
			 match-exp  nil)
		   (funcall illegal-lexeme self))
		  (eof-p ;; eof
		   nil)
		  (t
		   (ferror
		     "The lexer has achieved the impossible - miracles do happen.")))))
   unless skip-token
     return token
   when eof-p return nil))

(defmethod (grammar::map-regular-expressions-to-terminals lexer) (cfg)
  (loop for item being the array-elements of (lexer-State-to-regex self)
	when item do  (send item :resolve-to-terminal cfg)))

(defflavor lexers
	(active-lexer
	 lexer-list
	 lexer-sets
	 (time (time:print-current-time nil)))
	()
  (:default-handler lexers-to-lexer)
  :initable-instance-variables
  :writable-instance-variables
  :readable-instance-variables)

(defun lexers-to-lexer (self ignore message &rest arguments)
  (lexpr-funcall (lexers-active-lexer self) message arguments))

(defmethod (set-active-lexer lexers) (lexer)
  (setq active-lexer (cdr (assq lexer lexer-list)))
  (unless active-lexer
    (unless
      (cl:cerror
	"Choose a different lexer"
	(format nil
		"set-active-lexer passed ~A which is not one of the possible lexers:~
	           ~%  ~A"
		lexer
		;; (loop for (lexer-name . lexer-object) in lexer-list
		;;       collect lexer-name)
		(loop for lexer-name-.-lexer-object in lexer-list
		      collect (first lexer-name-.-lexer-object))))
      (setf active-lexer
	    (fquery `(:type :readline
		      :list-choices t
		      :choices ,(loop for lexer in lexer-list
				      collect (list lexer (string (car lexer)))))
		    "Select a lexer ")))))

(defun lexer-check-value-puthash (item new-value derivers-list hash-table)
  (let* ((value (gethash item hash-table))
	 (derivers (cdr (assq item derivers-list))))
    (when derivers (setq new-value (list* new-value derivers)))
    (cond
      ((null value)
       (puthash item new-value hash-table))
      ((equal value new-value))
      (t
       (if (and (or (null new-value) (null value))
		(or (and new-value (string-equal new-value "literal"))
		    (and value (string-equal value "literal"))))
	   (puthash item (or new-value value) hash-table)
	   (ferror "~%Terminal ~A has conflicting values: ~A and ~A"
		   item value new-value))))))

(defun get-grammar-symbol(thing)
  (or (and thing (symbolp thing) thing)
      (and (grammar::terminal-p thing) (send thing :name))
      (ferror "Junky terminal : ~A" thing)))

(defun literal-regex-string (string)
  (declare (special *lexer-operators* *lexer-escape-operator*))
  (and
    ;; Check whether the operators are used in a literal sense
     (loop for i = (string-search-set *lexer-operators* string) then
	       (string-search-set *lexer-operators* string (+ i 1))
	   while i
	   always (or (char-equal  (aref string i) *lexer-escape-operator*)
		      (and (not (zerop i))
			   (char-equal  (aref string (- i 1)) *lexer-escape-operator*))))
    (intern (string-upcase
	      (loop with new-string = (string-append "" string) with j = -1
		    for i from 0 below (string-length string)

		    do (if (char-equal (aref string i) *lexer-escape-operator*)
			   (setf (aref new-string (incf j)) (aref string (incf i)))
			   (setf (aref new-string (incf j)) (aref string i)))
		    finally
		      (return (nsubstring new-string 0 (+ 1 j))))))))

(defmethod (grammar::terminals lexers) (&optional (lexers :all))
  (loop with terminal-hash =  (cl:make-hash-table :test 'eq :mutating nil :size 200)
	for (lexer-name . lexer) in lexer-list
	as derivers-list = (get-derivers self)
	when (or (eq lexers :all) (memq lexer-name lexers))
	  do (loop for item in (lexer-pseudo-terminals lexer)
		   do (lexer-check-value-puthash item :literal derivers-list terminal-hash))

	     (loop for item being the array-elements of (lexer-State-to-regex  lexer)
		   when item
		     do (let ((symbol-or-terminal  (send item :terminal))
			      (hash-table (send item :keyword-hash-table))
			      (terminal-type (and (literal-regex-string (send item :string))
						  :literal)))
			  (when symbol-or-terminal
			    (lexer-check-value-puthash
			      (get-grammar-symbol symbol-or-terminal)
			      terminal-type
			      derivers-list
			      terminal-hash))
			  (when hash-table
			    (loop for symbol-or-terminal being the hash-elements of
				      hash-table
				  do (lexer-check-value-puthash
				       (get-grammar-symbol symbol-or-terminal)
				       :keyword
				       derivers-list
				       terminal-hash)))))
	finally
	  (return
	    (sort
	      (loop for value being the hash-elements of terminal-hash with-key item
		    collect (if (cl:consp value)
				(list* item :type value)
				(list item :type value)))
	      '(lambda (x y) (string-lessp (car x) (car y)))))))



;;; This takes a list of context free grammars and a lexers-object and
;;; maps all of the terminals in the lexers in the lexers-object to the
;;; corresponding grammars.
;;;
;;; The list of context free grammars is of the form:
;;;	((lexer-grammar-name-1 . grammar-global-name-1) ...
;;;	 (lexer-grammar-name-n . grammar-global-name-n))
;;; For the Pascal editor the list of context free grammars would be of the form:
;;;	((iso . *iso-ll-1-cfg*) (vs . *vs-ll-1-cfg*))
;;; And, the lexers-object would be of the form:
;;;	((iso . <iso-lexer-instance>) (vs . <vs-lexer-instance>)).
;;; The ordering of the 2 Pascal editor lists above is not guaranteed.
;;;
;;; The end result of this method is that instead of returning a symbol associated
;;; with a match the lexers' internal data structures (actually, the
;;; regular-expression-terminal IV found in the array in State-to-Regex IV of the
;;; lexer) will be modified so that the lexers will return the non-terminals of its
;;; corresponding grammar for the symbol.
;;;
;;; Thus, for the Pascal editor example, instead of the <iso-lexer-instance> and
;;; <vs-lexer-instance> both returning the symbol 'procedure, the <iso-lexer-instance>
;;; would return the *iso-ll-1-cfg* grammar non-terminal for procedure, and the
;;; <vs-lexer-instance> would return the *vs-ll-1-cfg* grammar non-terminal for
;;; procedure.
(defmethod (grammar::map-regular-expressions-of-grammars-to-terminals lexers) (grammars)
  (loop with matching-lexer
	for (name . grammar) in grammars
	do (unless (setq matching-lexer (assq name lexer-list))
	     (ferror "Lexer//parser mismatch:~
			~%The lexer object ~A does not contain a lexer named ~A.~
			~%It does contain the following lexer(s):~
			~%~{ ~A ~}."
		     self name
		     ;; (loop for (lexer-name . object) in lexer-list
		     ;;       collect lexer-name)
		     (loop for lexer-name-.-object in lexer-list
			   collect (first lexer-name-.-object))))
	   (grammar::map-regular-expressions-to-terminals
	     (cdr matching-lexer) (symeval grammar))))

;;; This takes a context free grammar and a lexers-object and maps all of the terminals
;;; in the lexers (note the plural) in the lexers-object to that one grammar.
;;; The end result is that instead of returning a symbol associated with a match the
;;; lexers will return the non-terminals of the grammar for the symbol.
(defmethod (grammar::map-regular-expressions-to-terminals lexers) (cfg)
  ;; (loop for (lexer-name . lexer) in lexer-list
  ;;       do (grammar::map-regular-expressions-to-terminals lexer cfg))
  (loop for lexer-name-.-lexer in lexer-list
	do (grammar::map-regular-expressions-to-terminals (cdr lexer-name-.-lexer) cfg)))

(defmethod (:def-actions lexers)()
  (loop for (lexer-name . lexer) in lexer-list nconc (send lexer :def-actions lexer-name)))

(defmethod (:def-actions lexer)(lexer-name)
  (loop with action-index = 0
	for regular-expression being the array-elements of State-to-regex
	as action = (and regular-expression
			 (send regular-expression :def-action lexer-name (incf action-index)))
	when action collect action))



;;; Utility to obtain the regular expression object, from a regex
;;; source string
(defmethod (:get-regex lexer) (regex-source-string)
  (loop for r being the array-elements of state-to-regex
	  when (and r (string-equal (send r :source-string) regex-source-string))
	    return r))

(defmethod (:get-lexer lexers)(lexer-name)
  (loop for (name . lexer) in lexer-list
	when (string-equal name lexer-name) return lexer))

(defmethod (collect-identifier lexer) (terminal keyword-hash-table)
  (loop while (< cursor buffer-size)
	for char = (aref buffer cursor)
	while (or (cl:alphanumericp char)
		  (char-equal #\_ char))
	do (incf cursor)
	finally (setq last-match cursor)
		(return
		  (or (and keyword-hash-table
			   (gethash (match-string self) keyword-hash-table))
		      terminal))))

(defun test-compiled-regex  (simple-automaton)
  (declare (values match-p eof-p length))
  (loop with automaton = (car simple-automaton)
	with rows = (array-dimension-n 1 automaton) and cols = (array-dimension-n 2 automaton)
	and goal and current-state = 0 and last-match-length = 0
	for char = (let ((c (send zl:standard-input :tyi)))
		     (when c (send zl:standard-output :tyo c)) (char-code c))
	for token-length from 0
	while (and char
		   (setq current-state
			 (and (< char cols)
			      (let ((state (aref automaton current-state char)))
				(when (memq state (cdr simple-automaton))
				    (setq goal state
					  last-match-length (+ token-length 1)))
				(when (< state rows)
				  state)))))
	finally (return (values goal (null char) last-match-length goal))))
