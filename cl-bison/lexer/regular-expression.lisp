;;; -*- Syntax: Common-Lisp; Package: LEXER; Base: 10; Mode: LISP -*-

(defflavor regular-expression
	((string)
	 (action nil)
	 (keyword-hash-table nil)
	 (terminal nil)
	 (source-string ""))
	()
  :initable-instance-variables
  :writable-instance-variables
  :readable-instance-variables
  :gettable-instance-variables :settable-instance-variables)

(defmethod (:print-self regular-expression)
	   (stream ignore ignore)
  (sys:printing-random-object (self stream :typep)
    (format stream "~S" source-string)))

(defmethod (:def-action regular-expression) (lexer-name action-number &optional action-name )
  (declare (special *lexer-action-hash*))
  (cond
    ((memq action `(:skip :identifier :return-terminal)) nil)
    ((and (cl:consp action) (memq (car action) `(:skip :identifier :return-terminal)))
     (setq action (car action))
     nil)
    ((and (listp action) (eq (car action) :action-function))
     D,#TD1PsT[Begin using 006 escapes](1 0 (NIL 0) (NIL :ITALIC NIL) "CPTFONTI");; An action function that is user-defined.
0     (setq action (second action)))
    ((and action (listp action) (and *lexer-action-hash*
				     (scl:gethash action *lexer-action-hash*)) )
     (setq action (scl:gethash action *lexer-action-hash*))
     nil)
    ((cl:consp action)
     (setq action-name (or action-name
			   (intern (format nil "~A-lex-action-~A" lexer-name action-number))))
     (let* ((action-fun `(defun ,action-name ($$grammar-symbol$$)
			   $$grammar-symbol$$ (comment ,string)
			   ,(if keyword-hash-table
				`(let (($$grammar-symbol$$
					 (or (gethash (match-string *lexer*)
						      ,keyword-hash-table)
					     $$grammar-symbol$$)))
				   $$grammar-symbol$$
				   ,@action)
				`(progn ,@action)))))
       (when *lexer-action-hash*
	 (setf (scl:gethash action *lexer-action-hash*) action-name))
       (setq action action-name)
       action-fun))
    ((symbolp action)
     nil)
    (t (ferror "Could not understand this action: ~A" action) )))

(defmethod (:execute-action regular-expression) (&optional (lexer *lexer*))
  (cond
    ((eq action :skip)
     (continue-lexer lexer))
    ((eq action :return-terminal)
     (or (and keyword-hash-table (gethash (match-string lexer) keyword-hash-table))
	 terminal))
;    ((eq action :identifier)
;     (collect-identifier lexer terminal keyword-hash-table))
    (action
     (let ((*lexer* lexer))
       (funcall action terminal)))
    (t terminal)))


(defun resolve-regular-expression-symbol(cfg symbol-or-terminal)
  (let ((grammar-symbol (get-grammar-symbol symbol-or-terminal)))
    (and  grammar-symbol
	  (or (send cfg :name-to-symbol grammar-symbol)
	      (ferror "Could not resolve the terminal \"~A\" in the lexer to ~
                                 the corresponding one in the parser. ~
				 ~%The lexer and parser are inconsistent."
		      symbol-or-terminal)))))


  
(defmethod (:resolve-to-terminal regular-expression)(cfg)
  (cond-every 
    (terminal (setq terminal (resolve-regular-expression-symbol cfg terminal)))
    
    (keyword-hash-table
      (loop for item being the hash-elements of keyword-hash-table with-key key
	    as keyword-terminal = (resolve-regular-expression-symbol cfg item)
	    do (send keyword-hash-table :put-hash key keyword-terminal)))))



;(let ((old-name (scl:gethash action *lexer-action-hash*)))
;     (declare (special *lexer-action-hash*))
;     (setq action-name   (or action-name
;			     old-name
;			     (intern
;			       (format nil "~A-lex-action-~A" lexer-name action-number))))
;     (if (eq old-name action-name)
;	 (progn (setq action action-name) nil)
;	 (let* ((action-fun `(defun ,action-name ($$grammar-symbol$$)
;			       $$grammar-symbol$$ (comment ,string)
;			       ,(if keyword-hash-table
;				    `(let (($$grammar-symbol$$
;					     (or (gethash (match-string *lexer*)
;							  ,keyword-hash-table)
;						 $$grammar-symbol$$)))
;					  $$grammar-symbol$$
;					  ,@action)
;				    `(progn ,@action)))))
;	       (setf (scl:gethash action *lexer-action-hash*) action-name)
;	       (setq action action-name)
;	       action-fun)))