;;; -*- Mode: LISP; Package: LEXER; Lowercase: T; Base: 10; Syntax: Zetalisp -*-

;;;>
;;;> *****************************************************************************************
;;;> ** (c) Copyright 1993-1982 Symbolics, Inc.  All rights reserved.
;;;> ** Portions of font library Copyright (c) 1984 Bitstream, Inc.  All Rights Reserved.
;;;>
;;;>    The software, data, and information contained herein are proprietary 
;;;> to, and comprise valuable trade secrets of, Symbolics, Inc., which intends 
;;;> to keep such software, data, and information confidential and to preserve 
;;;> them as trade secrets.  They are given in confidence by Symbolics pursuant 
;;;> to a written license agreement, and may be used, copied, transmitted, and 
;;;> stored only in accordance with the terms of such license.
;;;> 
;;;> Symbolics, Symbolics 3600, Symbolics 3670 (R), Symbolics 3675 (R), Symbolics
;;;> 3630, Symbolics 3640, Symbolics 3645 (R), Symbolics 3650 (R), Symbolics 3653,
;;;> Symbolics 3620 (R), Symbolics 3610 (R), Symbolics Common Lisp (R),
;;;> Symbolics-Lisp (R), Zetalisp (R), Genera (R), Wheels (R), Dynamic Windows (R),
;;;> SmartStore (R), Semanticue (R), Frame-Up (R), Firewall (R), Document Examiner (R),
;;;> Delivery Document Examiner, "Your Next Step in Computing" (R), Ivory, MacIvory,
;;;> MacIvory model 1, MacIvory model 2, MacIvory model 3, XL400, XL1200, XL1201,
;;;> Symbolics UX400S, Symbolics UX1200S, NXP1000, Symbolics C, Symbolics Pascal (R),
;;;> Symbolics Prolog, Symbolics Fortran (R), CLOE (R), CLOE Application Generator,
;;;> CLOE Developer, CLOE Runtime, Common Lisp Developer, Symbolics Concordia, Joshua,
;;;> Statice (R), and Minima are trademarks of Symbolics, Inc.
;;;> 
;;;> RESTRICTED RIGHTS LEGEND
;;;>    Use, duplication, and disclosure by the Government are subject to restrictions 
;;;> as set forth in subdivision (c)(1)(ii) of the Rights in Technical Data and Computer 
;;;> Software Clause at DFAR 52.227-7013.
;;;> 
;;;>      Symbolics, Inc.
;;;>      6 Concord Farms
;;;>      555 Virginia Road
;;;>      Concord, Massachusetts 01742-2727
;;;>      United States of America
;;;>      508-287-1000
;;;>
;;;> *****************************************************************************************
;;;>

;; Get the first token of a line in the permanent plist
(defsubst get-line-token (line)
  (zwei:line-token line))

;; Set the first token of a line in the permanent plist
(defsubst set-line-token (line token)
  (setf (zwei:line-token line) token))

;; The flavor holding state for reading in tokens from the fsm
(defflavor zmacs-tokenizer
	((editor-buffer zwei:*interval*)	;Input buffer if no stream is in use
	 (verbose-p nil) 			;Verbose mode?
	 dialect
	 (grammar-symbol nil))	        	;Only valid when state is a symbol
	(zmacs-lexer)
  :abstract-flavor
  :settable-instance-variables
  :gettable-instance-variables
  :initable-instance-variables)

(defflavor tokenizer-with-cfg
	((cfg nil)			;if a parser is being used this field will be non-nil
	 (saved-token-list nil)		;tokens waiting to be added to the free token list
	 (last-saved-token nil)		;last token in the saved token list
	 (first-free-token nil)		;first token in the free token list
	 (last-free-token nil)		;last token in the free token list
	 (saved-template-token-list nil);template tokens waiting to be added to the free
	 (last-saved-template-token nil);last template-token in the saved template-token list
	 (first-free-template-token nil);first template-token in the free template-token list
	 (last-free-template-token nil)	;last template-token in the free template-token list
	 ;; multi-line tokens waiting to be added to the free multi-line token list
	 (saved-multi-line-token-list nil)
	 ;; last multi-line-token in the saved multi-line-token list 
	 (last-saved-multi-line-token nil)
	 ;; first multi-line-token in the free multi-line-token list
	 (first-free-multi-line-token nil)
	 ;; last multi-line-token in the free multi-line-token list
	 (last-free-multi-line-token nil)
	 ;; during sectionization we don't wait to recycle tokens
	 (recycle-tokens-immediately-p nil)
	 (outgoing-state nil)  		D,#TD1PsT[Begin using 006 escapes](1 0 (NIL 0) (NIL :ITALIC NIL) "CPTFONTI"); the outgoing lexer state exiting from multi-line
0	 				1; comments. It is normally set by the comment processor
0	 				1; or the continuation character and triggers the
0	 				1; creation of a multi-line-token
0	 )
	(zmacs-tokenizer)
  :abstract-flavor
  :settable-instance-variables
  :gettable-instance-variables
  :initable-instance-variables)

(defmethod (line-has-continuation-char-p zmacs-tokenizer)(ignore)
  nil)

(defmethod (:get-end-of-token tokenizer-with-cfg) (syntactic-token)
  (loop for current-token = syntactic-token then sibling
	as sibling = (send current-token :sibling)
	while sibling
	finally (return current-token)))

(defun-in-flavor (new-syntactic-token tokenizer-with-cfg)(symbol parent start end)
  (if first-free-token
      (let ((return-value first-free-token))
	(unless (setq first-free-token (send first-free-token :next))
	  (setq last-free-token nil))
	(send return-value :re-initialize symbol parent start end))
      (let ((token (grammar::make-syntactic-token symbol parent)))
	(send token :set-start-end-indices start end)
	token)))

(defun-in-flavor (new-template-token tokenizer-with-cfg)(parent start end)
  (if first-free-template-token
      (let ((return-value first-free-template-token))
	(unless (setq first-free-template-token (send first-free-template-token :next))
	  (setq last-free-template-token nil))
	(send return-value :re-initialize nil parent start end))
      (let ((token (grammar::make-syntactic-template-token parent)))
	(send token :set-start-end-indices start end)
	token)))

(defun-in-flavor (new-multi-line-token tokenizer-with-cfg)(symbol parent start end)
  (if first-free-multi-line-token
      (let ((return-value first-free-multi-line-token))
	(unless (setq first-free-multi-line-token (send first-free-multi-line-token :next))
	  (setq last-free-multi-line-token nil))
	(send return-value :re-initialize symbol parent start end))
      (let ((token (grammar::make-multi-line-syntactic-token symbol parent)))
	(send token :set-start-end-indices start end)
	token)))

;; This methods returns some kind of token instance.  It either returns the
;; first token in the free list (after initializing it) or makes a new instance.
;; This method along with :FREE-SYNTACTIC-TOKEN-LIST maintain the free syntactic
;; token lists.
(defmethod (new-token tokenizer-with-cfg) (token-type symbol parent start end)
  (selectq token-type
    (grammar::syntactic-token
     (new-syntactic-token symbol parent start end))
    (grammar::multi-line-syntactic-token
     (new-multi-line-token symbol parent start end))
    (grammar::syntactic-template-token
     (new-template-token parent start end))))


;; This method takes a line & adds its tokens to the saved free token list.  It then resets
;; the line's line-mung-condition on the line-plist.
;; This method along with NEW-SYNTACTIC-TOKEN & :append-syntactic-list maintains the
;; free syntactic token lists.
(defmethod (:free-syntactic-token-list tokenizer-with-cfg)
	   (free-line &optional (last-syntactic-token-on-line nil))
  (if last-syntactic-token-on-line
      (progn
	(send last-syntactic-token-on-line :set-next saved-token-list)
	(unless saved-token-list
	  (setq last-saved-token last-syntactic-token-on-line))
	(setq saved-token-list (get-line-token free-line)))
    (loop for token = (get-line-token free-line) then next-token
	  while (and token (typep token 'grammar::syntactic-token))
	  as next-token = (send token :next)
	  do (grammar::top-level-typecase token
	       (grammar::syntactic-template-token
		 (send token :set-next saved-template-token-list)
		 (unless saved-template-token-list (setq last-saved-template-token token))
		 (setq saved-template-token-list token))
	       (grammar::multi-line-syntactic-token
		 (send token :set-next saved-multi-line-token-list)
		 (unless saved-multi-line-token-list
		   (setq last-saved-multi-line-token token))
		 (setq saved-multi-line-token-list token))
	       (grammar::syntactic-token
		 (send token :set-next saved-token-list)
		 (unless saved-token-list
		   (setq last-saved-token token))
		 (setq saved-token-list token)))))
  ;; Initialize the line mung condition - the line is no longer munged.
  (putprop (locf (zwei:line-plist free-line)) NIL 'zwei:line-mung-condition)
  (setf (zwei:line-token free-line) nil)
  ;; If we are sectionizing the buffer, tokens can be recycled immediately. 
  (when recycle-tokens-immediately-p
    (send self :append-syntactic-token-list)))

;; This method takes a line & adds its tokens to the free token lists.  This
;; method along with NEW-SYNTACTIC-TOKEN & :free-syntactic-list maintains
;; the free syntactic token lists.
(defmethod (:append-syntactic-token-list tokenizer-with-cfg) ()
  (when saved-multi-line-token-list
    (if last-free-multi-line-token
	(send last-free-multi-line-token :set-next saved-multi-line-token-list)
      (setf first-free-multi-line-token saved-multi-line-token-list))
    (setq last-free-multi-line-token last-saved-multi-line-token)
    (setq saved-multi-line-token-list nil
	  last-saved-multi-line-token nil))
  (when saved-template-token-list
    (if last-free-template-token
	(send last-free-template-token :set-next saved-template-token-list)
      (setf first-free-template-token saved-template-token-list))
    (setq last-free-template-token last-saved-template-token)
    (setq saved-template-token-list nil
	  last-saved-template-token nil))
  (when saved-token-list
    (if last-free-token
	(send last-free-token :set-next saved-token-list)
      (setf first-free-token saved-token-list))
    (setq last-free-token last-saved-token)
    (setq saved-token-list nil
	  last-saved-token nil)))

(defmethod (set-ivs tokenizer-with-cfg)
	   (lexer cfg-object)
  (let ((lexer-list (lexers-lexer-list lexer)))
    1;; The caar below0 1presumes that there is only 1 lexer in the lexer object
0    (lexer::set-active-lexer lexer (caar lexer-list))
    (setq cfg			 	 cfg-object
	  start-state		 	 (lexer-start-state lexer)
	  State-to-regex	 	 (lexer-State-to-regex lexer)
	  grammar::comb		 	 (grammar::comb-comb lexer)
	  grammar::valid-entry-matrix	 (grammar::comb-valid-entry-matrix lexer)
	  grammar::comb-index-array	 (grammar::comb-comb-index-array lexer)
	  1;; Initialize all of the common0 1pseudo-terminals as IV's - making them fast
0	  1;; to0 1access0 1while0 1editing
	  ;;
	  ;; IV's of zmacs-lexer
0	  bof-terminal		 	 (resolve-regular-expression-symbol cfg 'bof)
	  eof-terminal		 	 (resolve-regular-expression-symbol cfg 'eof)
1;0	  line-char-terminal	 	 (resolve-regular-expression-symbol cfg 'line-char)
	  begin-token-terminal	     (resolve-regular-expression-symbol cfg 'begin-token)
	  illegal-lexeme-terminal    (resolve-regular-expression-symbol cfg 'illegal-lexeme)
	  1;; Initialize this IV as it is used to calculate whether to return itself or one
0	  1;; of the pseudo-terminals for comments -- IV of zmacs-lexer
0	  comment-terminal	     (resolve-regular-expression-symbol cfg 'comment))
    (set-additional-ivs self cfg)
    (setq state start-state)))

(defmethod (set-additional-ivs tokenizer-with-cfg) (cfg)
  cfg ;; ignore
  )

;; Initialize line-related fields of token-sequence
(defmethod (:initialize-for-new-line zmacs-tokenizer) (new-line)
  (setq start		 0
	last-match	 0
	end-of-lookahead 0
	cursor		 0
	eof-p		 nil
	buffer-size	 (array-active-length new-line)))

;;; Get the string for the current token
;;;
(defmethod (:current-token-string zmacs-tokenizer) ()
  (substring buffer start cursor))


;; Get the next line from the editor buffer.
(defun-in-flavor (get-next-line zmacs-tokenizer) ()
  (when (setq buffer (zwei:line-next buffer))
    (send self ':initialize-for-new-line buffer)))

(defmethod (:print-line-cursor zmacs-tokenizer) ()
  (let ((array-of-blanks (make-array 190. :type 'sys:art-string
					  :initial-value #/Space)))
    (setf (aref array-of-blanks start) #/)
    (when ( start cursor)
      (setf (aref array-of-blanks cursor) #/))
    (format t "~& LINE: ~A" buffer) 
    (format t "~&       ~A" array-of-blanks)
    (setf (aref array-of-blanks start) #/Space)
    (setf (aref array-of-blanks cursor) #/Space)))

(defmethod (initialize-lexer-to-start-of-line tokenizer-with-cfg) (line initial-state)
  (ignore initial-state)
  (setq state 		start-state
	buffer 		line
	start 		0
	cursor 		0
	last-match 	0
	skip-token  	nil
	eof-p           nil
	outgoing-state  start-state
	buffer-size     (zwei:line-length line)))

1;;; Perform the token-level initializations necessary to initiate recognize a token
0(defmethod (initialize-lexer-for-token tokenizer-with-cfg)()
  (initialize-lexer-pos)
  (setq state     start-state))

1;;; 
;;; Method to tokenize one or more lines beginning with start line, and lexing the specified
;;; number of lines.
;;;
0(defmethod (tokenize-lines tokenizer-with-cfg) (parent start-line number-of-lines)
  (declare (sys:array-register fast-State-to-regex fast-comb-index-array fast-comb
			       fast-buffer))
  (loop initially
	  (setq outgoing-state nil)
	with last-token
	with fast-State-to-regex = State-to-regex
	and fast-comb-index-array = grammar::comb-index-array
	and fast-comb = grammar::comb
	and grammar:valid-entry-matrix = grammar:valid-entry-matrix

	for line = start-line then (zwei:line-next line)
	while line
	for count below number-of-lines
	do (cl:psetf
	     (cl:getf (zwei:line-contents-plist line) 'zwei:presentation-nodes) nil
	     (cl:getf (zwei:line-contents-plist line) 'zwei:raw-presentation-nodes) nil)
	   1;;0 1Initialize the line mung condition, the line is no longer munged.
0	   (send self :free-syntactic-token-list line)
	   (loop with fast-buffer = line and syntactic-token = line
		 and match-state and incoming-state
		 and line-continuation-char-pos = (line-has-continuation-char-p self line)
		 
		 as  grammar-symbol = nil and previous-syntactic-token = syntactic-token
		 initially
1		   ;; Initialize for start of line
0		   (initialize-lexer-to-start-of-line self line outgoing-state)
1		   ;; Check whether there is a multi-line token on the line
0		   (when (and ( state start-state) (aref fast-State-to-regex state))
		     (setq match-state  state))
		   
		   (setq incoming-state state)
		   
		 do (loop with buffer-size = buffer-size
			  while (setq state (next-zmacs-lexer-state
					      state
					      (get-char-from-fsm
						cursor buffer-size fast-buffer)))
			  when (aref fast-State-to-regex state)
			    do (setq last-match cursor
				     match-state state)
			  finally
			    (setq grammar-symbol (get-fsm-symbol
						   match-state cursor last-match start
						   illegal-lexeme-terminal illegal-lexeme))
			    (if skip-token
				(setq outgoing-state start-state)		     
1				;; A nil grammar symbol indicates end of line
0				(when grammar-symbol
1				  ;; The comment action routine sets up the "state"
0				  (if (eq (send grammar-symbol :name) 'grammar::template)
				      (setq syntactic-token
					    (grammar:initialize
					      (new-template-token parent start last-match)
					      cfg self (nsubstring buffer start cursor)))
				      (progn
					(setq syntactic-token
					      (new-syntactic-token grammar-symbol
								   parent start last-match))
					(send syntactic-token :set-symbol grammar-symbol)))
				  
				  (if (eq previous-syntactic-token fast-buffer)
				      (set-line-token fast-buffer syntactic-token)
				      (send previous-syntactic-token :set-next
					    syntactic-token))))
			    
;		     (format t "~%line ~A Symbol ~A  in: ~A out: ~A cursor:~A lchar-pos: ~A"
;			     fast-buffer
;			    grammar-symbol incoming-state outgoing-state cursor line-continuation-char-pos)
			    (when (eq cursor line-continuation-char-pos)
			      (setq outgoing-state
				    (if skip-token 
					start-state
					(get-fsm-state-for-string self fast-buffer
								  start cursor incoming-state)))
			      (setq grammar-symbol nil)
			      (return nil))
1			    ;; Initialize the lexer for the next iteration of the token loop
0			    (initialize-lexer-for-token self)
			    (setq match-state nil))
		 while grammar-symbol
		 finally
		   (if (eq syntactic-token fast-buffer)
		       (set-line-token fast-buffer fast-buffer)
		       (send syntactic-token :set-next fast-buffer))
		   (let ((token (get-line-token fast-buffer)))
		     1;; If either the incoming or outgoing state is not the start state
0		     1;; there is atleast one token on the line (even if it is empty).
0		     (cond-every
		       ((neq incoming-state start-state)
			1;; Transform the first token into a mid or end token.
0			(grammar:change-syntactic-token-into-multi-line
			  token
			  (if (and1 0(eq token syntactic-token)	1; Only token on the line
0				   (neq outgoing-state start-state))
			      mid-token-terminal end-token-terminal)
			  fast-buffer incoming-state outgoing-state self)
1			;; Only token on the line, it was replaced, so adjust the last token
0			(when (eq token syntactic-token)
			  (setq syntactic-token (get-line-token fast-buffer))))

		       ((and (neq outgoing-state start-state)
			     (let ((type (send syntactic-token :type)))
			       (and (neq type mid-token-terminal)
				    (neq type end-token-terminal))))
			1;; Transform the0 1last token to be a multi line token if the outgoing
			;; state0 1is not the start state
0			(setq syntactic-token
			      (grammar:change-syntactic-token-into-multi-line     
				syntactic-token begin-token-terminal fast-buffer
				incoming-state outgoing-state self)))))
		   (setq last-token syntactic-token))
	finally
	  (return (values last-token line)))) 


(defmethod (:lex-lines tokenizer-with-cfg)
	   (first-line last-line &optional (parent nil))
  (tokenize-lines
    self parent first-line
    (loop for i upfrom 1
	  for line = first-line then (zwei:line-next line)
	  while line
	  until (eq line last-line)
	  finally (return i)))
  last-line)

;;; 
;;; 1This generic function finds potential matches for a blinker.  This path should only 
;;; be traversed when the cursor is on a "{", a "[", or a "(", or immediately to the
;;; right of a "}", a "]", or a ")" AND the current line has been munged (i.e. its 
;;; line-token list is not necessarily up to date). We don't want to do a 
;;; flush-mung-region-array every time a paren is typed into the editor.
;;;
;;; What is returned is a list of conses of the form:
;;;	 (grammar-terminal-of-interest . index-of-terminal-on-line)
;;;
;;; The real workhorse is symbol-to-bp-for-blinker, this routine just prunes and re-orders
;;; the return value as needed
;;; 
0(defmethod (blinker-token-list tokenizer-with-cfg) (bp direction)
  (let* ((bp-index (zwei:bp-index bp))
	 (tokens-on-line
	   (symbol-to-bp-for-blinker self bp (if (eq direction :next-token)
						 (zwei:line-length (zwei:bp-line bp))
						 bp-index))))
    (cond
      ((eq direction :prev-token)
       1;; if we are going backwards in the buffer, we want the tokens from the beginning
0       1;; of the line to the bp0.
       (nreverse tokens-on-line))
      ((null tokens-on-line)
       nil)
      (t
       1;; if we are going forwards in the buffer, we want the tokens from
0       1;; the bp to the end of the line0.
       (loop for return-list = tokens-on-line then rest
	     for (token-and-index . rest) on tokens-on-line
	     as index = (cdr token-and-index)
	     when (= index bp-index) return return-list)))))

1;;;
;;; This is just a version of the generic function (symbol-at-bp tokenizer-with-cfg)
;;; for use in the blinker process.  It finds potential matches (note the
;;; 'grammar:( |)| |(| |[| |]| |{| |}| below) for a blinker.  This path should only 
;;; be traversed when the cursor is on a "{", a "[", or a "(", or immediately to the
;;; right of a "}", a "]", or a ")" AND the current line has been munged (i.e. its 
;;; line-token list is not necessarily up to date). We don't want to do a 
;;; flush-mung-region-array every time a paren is typed into the editor.
;;;
;;; What is returned is a list of conses of the form:
;;;	 (grammar-terminal-of-interest . index-of-terminal-on-line)
;;;
0(defmethod (symbol-to-bp-for-blinker tokenizer-with-cfg) (bp bp-index)
  (declare (sys:array-register fast-State-to-regex fast-buffer
			       fast-comb-index-array fast-comb))
  (loop with fast-State-to-regex = State-to-regex
	with fast-buffer = (zwei:bp-line bp)
	with fast-comb-index-array = grammar::comb-index-array
	with fast-comb = grammar::comb
	with match-exp and reg-exp
	with index = bp-index
	as  grammar-symbol = nil
	initially
1	  ;; Initialize for start of line
0	  (initialize-lexer-to-start-of-line self fast-buffer nil)
1	  ;; Let's handle those multi-line tokens, we start the line in a matched state
0	  (when (and ( state start-state) (setq reg-exp (aref fast-State-to-regex state)))
	    (setq match-exp  reg-exp))
	do (loop while (setq state (next-zmacs-lexer-state
				     state
				     (get-char-from-fsm cursor buffer-size fast-buffer)))
		 when (setq reg-exp (aref fast-State-to-regex state))
		   do (setq last-match cursor
			    match-exp  reg-exp)
		 finally
		   (setq grammar-symbol (get-grammar-symbol-from-fsm
					  match-exp cursor last-match start
					  line-char-terminal
					  illegal-lexeme-terminal illegal-lexeme))
1		   ;; Now initialize the lexer for the next iteration of the token loop
0		   (initialize-lexer-for-token self)
		   (setq match-exp nil
			 reg-exp   nil))
	while grammar-symbol
	when (and (typep grammar-symbol 'grammar::terminal)
		  (memq (intern-soft (send grammar-symbol :name) 'grammar)
			'grammar:( |)| |(| |[| |]| |{| |}| )))
	  collect `(,grammar-symbol . ,(1- cursor))
	until ( cursor index)))


1;;; Get the grammar symbol at bp
0(defmethod (symbol-at-bp tokenizer-with-cfg) (bp)
  (declare (sys:array-register fast-State-to-regex fast-buffer
			       fast-comb-index-array fast-comb))
  (loop with fast-State-to-regex = State-to-regex
	with fast-buffer = (zwei:bp-line bp)
	with fast-comb-index-array = grammar::comb-index-array
	with fast-comb = grammar::comb
	with match-exp and reg-exp
	with index = (zwei:bp-index bp)
	as  grammar-symbol = nil
	initially
1	  ;; Initialize for start of line
0	  (initialize-lexer-to-start-of-line self fast-buffer nil)
1	  ;; Let's handle those multi-line tokens, we start the line in a matched state
0	  (when (and ( state start-state) (setq reg-exp (aref fast-State-to-regex state)))
	    (setq match-exp  reg-exp))
	  
	do (loop while (setq state (next-zmacs-lexer-state
				     state
				     (get-char-from-fsm cursor buffer-size fast-buffer)))
		 when (setq reg-exp (aref fast-State-to-regex state))
		   do (setq last-match cursor
			    match-exp  reg-exp)
		 finally
		   (setq grammar-symbol (get-grammar-symbol-from-fsm
					  match-exp cursor last-match start
					  line-char-terminal
					  illegal-lexeme-terminal illegal-lexeme))
1		   ;; Now initialize the lexer for the next iteration of the token loop
0		   (initialize-lexer-for-token self)
		   (setq match-exp nil
			 reg-exp   nil))
	while grammar-symbol
	when ( cursor index) return grammar-symbol
	finally (return nil)))

1;;;
;;; This method is used to obtain successive grammar symbols from an editor line
;;; All callers must be sure to initially call initialize-lexer-to-start-of-line
;;; and subsequently call initialize-lexer-for-token, before each call to this lexing
;;; routine.
;;;
0(defmethod (next-symbol-in-line tokenizer-with-cfg) ()
  (declare (sys:array-register fast-State-to-regex fast-buffer
			       fast-comb-index-array fast-comb))
  (loop with fast-State-to-regex = State-to-regex
	with fast-buffer = buffer
	with fast-comb-index-array = grammar::comb-index-array
	with fast-comb = grammar::comb
	with match-exp and reg-exp
	as  grammar-symbol = nil
	initially 
1	  ;; Let's handle those multi-line tokens, we start the line in a matched state
0	  (when (and ( state start-state) (setq reg-exp (aref fast-State-to-regex state)))
	    (setq match-exp  reg-exp))
	  
	do (loop while (setq state (next-zmacs-lexer-state
				     state
				     (get-char-from-fsm cursor buffer-size fast-buffer)))
		 when (setq reg-exp (aref fast-State-to-regex state))
		   do (setq last-match cursor
			    match-exp  reg-exp)
		 finally
		   (setq grammar-symbol (get-grammar-symbol-from-fsm
					  match-exp cursor last-match start
					  line-char-terminal
					  illegal-lexeme-terminal illegal-lexeme)))
	while skip-token
1	;; Now initialize the lexer for the next iteration of the token loop
0	do (initialize-lexer-for-token self)
	   (setq match-exp nil
		 reg-exp   nil)
	finally (return grammar-symbol)))
