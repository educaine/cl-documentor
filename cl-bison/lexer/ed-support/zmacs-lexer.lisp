;;; -*- Syntax: Zetalisp; Package: LEXER; Base: 10; Mode: LISP -*-
;;;>
;;;> *****************************************************************************************
;;;> ** (c) Copyright 1993 Symbolics, Inc.  All rights reserved.
;;;> ** Portions of font library Copyright (c) 1984 Bitstream, Inc.  All Rights Reserved.
;;;>
;;;>    The software, data, and information contained herein are proprietary 
;;;> to, and comprise valuable trade secrets of, Symbolics, Inc., which intends 
;;;> to keep such software, data, and information confidential and to preserve 
;;;> them as trade secrets.  They are given in confidence by Symbolics pursuant 
;;;> to a written license agreement, and may be used, copied, transmitted, and 
;;;> stored only in accordance with the terms of such license.
;;;> 
;;;> Symbolics, Symbolics 3600, Symbolics 3670 (R), Symbolics 3675 (R), Symbolics
;;;> 3630, Symbolics 3640, Symbolics 3645 (R), Symbolics 3650 (R), Symbolics 3653,
;;;> Symbolics 3620 (R), Symbolics 3610 (R), Symbolics Common Lisp (R),
;;;> Symbolics-Lisp (R), Zetalisp (R), Genera (R), Wheels (R), Dynamic Windows (R),
;;;> SmartStore (R), Semanticue (R), Frame-Up (R), Firewall (R), Document Examiner (R),
;;;> Delivery Document Examiner, "Your Next Step in Computing" (R), Ivory, MacIvory,
;;;> MacIvory model 1, MacIvory model 2, MacIvory model 3, XL400, XL1200, XL1201,
;;;> Symbolics UX400S, Symbolics UX1200S, NXP1000, Symbolics C, Symbolics Pascal (R),
;;;> Symbolics Prolog, Symbolics Fortran (R), CLOE (R), CLOE Application Generator,
;;;> CLOE Developer, CLOE Runtime, Common Lisp Developer, Symbolics Concordia, Joshua,
;;;> Statice (R), and Minima are trademarks of Symbolics, Inc.
;;;> 
;;;> RESTRICTED RIGHTS LEGEND
;;;>    Use, duplication, and disclosure by the Government are subject to restrictions 
;;;> as set forth in subdivision (c)(1)(ii) of the Rights in Technical Data and Computer 
;;;> Software Clause at DFAR 52.227-7013.
;;;> 
;;;>      Symbolics, Inc.
;;;>      6 Concord Farms
;;;>      555 Virginia Road
;;;>      Concord, Massachusetts 01742-2727
;;;>      United States of America
;;;>      508-287-1000
;;;>
;;;> *****************************************************************************************
;;;>

D,#TD1PsT[Begin using 006 escapes](1 0 (NIL 0) (NIL :ITALIC NIL) "CPTFONTI");;;
;;; 0		1LEXER definition
;;;

0(defflavor zmacs-lexer
	((state 0)	;; The internal lexer state - used to calculate state for
	 		;; tokens which span lines.
	 (eof-terminal 'eof)
	 (bof-terminal 'bof)
	 (line-char-terminal 'line-char)
	 (comment-terminal 'comment)

	 1;; the following ivs are only used by multi-line-comment-lexers
	 0(begin-token-terminal 'begin-token)  
	 (mid-token-terminal nil)
	 (end-token-terminal nil)

	 (illegal-lexeme-terminal nil)
	 (derivers-list nil)
	 )
	(lexer)
  :initable-instance-variables
  :writable-instance-variables
  :readable-instance-variables
  )

(defmethod (handles-derivers-p zmacs-lexer) () t)
(defmethod (get-derivers zmacs-lexer) () (zmacs-lexer-derivers-list self))
(defmethod (set-derivers zmacs-lexer) (derivers-list)
  (setf (zmacs-lexer-derivers-list self) derivers-list))

1;;;
;;; This method may be used to obtain the actual string that was matched by the lexer
;;;
;;; requires
;;;    A match has been made or an illegal token detected
;;;
;;; effects
;;;0   1returns a (2 0 (NIL 0) (NIL :BOLD NIL) "CPTFONTCB")pointer(nsubstring)1 to the string that was accepted/rejected.
;;;
0(defmethod (match-string zmacs-lexer) ()
  (let ((match-start (cond
		       (( (1+ start) buffer-size) 0)
		       ((null stream) start)
		       (t
			(1+ start))))
	(match-end  (if (and stream eof-p) (1+ cursor) cursor)))
    (nsubstring buffer match-start match-end)))




(defmacro next-zmacs-lexer-state (row col &environment env)
  (once-only (col &environment env)
    `(unless (zerop (aref grammar::valid-entry-matrix ,row ,col))
	 (aref fast-comb (+ (aref fast-comb-index-array ,row) ,col)))))

(defmacro get-grammar-symbol-from-fsm
	  (match-exp cursor last-match start line-char-terminal
	   illegal-lexeme-terminal illegal-lexeme)
  `(cond
    (,match-exp
     1;; Made a match. Execute the associated actions.
0     (setq ,cursor ,last-match)
     (let ((result
	     (execute-lexer-actions ,match-exp self)))
       (if (eq result ,line-char-terminal) nil result)))
    (( ,cursor ,start)
     1;; No match, invoke the illegal lexeme processor
0     (setq ,last-match ,cursor
	   ,match-exp  nil)
     (cond
       (,illegal-lexeme-terminal
	,illegal-lexeme-terminal)
       (t
	(funcall ,illegal-lexeme self))))))

(defmacro get-fsm-symbol
	  (match-state cursor last-match start illegal-lexeme-terminal illegal-lexeme)
  `(cond
    (,match-state
     1;; Made a match. Execute the associated actions.
0     (setq ,cursor ,last-match)
     (execute-lexer-actions (aref fast-state-to-regex ,match-state) self))
    (( ,cursor ,start)
     1;; No match, invoke the illegal lexeme processor
0     (setq ,last-match ,cursor
	   ,match-state  nil)
     (cond
       (,illegal-lexeme-terminal
	,illegal-lexeme-terminal)
       (t
	(funcall ,illegal-lexeme self))))))


(defmacro get-char-from-fsm (cursor buffer-size fast-buffer)
  `(cond
       (( ,cursor ,buffer-size)
	;; This is an end-of-line check -- rarer than the fall thru option
	(if ( ,cursor ,buffer-size)
	    ;; We're done with the line & already seen the #\return.
	    (loop-finish)
	    (incf ,cursor)
	    (char-code #\return)))
       (t (prog1 (let ((num (char-code (aref ,fast-buffer ,cursor))))
		   (if (> num 255) 255 num))
		 (incf ,cursor)))))
