;;; -*- Syntax: Zetalisp; Package: LEXER; Base: 10; Mode: Lisp -*-

(defflavor multi-line-tokenizer
	()
	(tokenizer-with-cfg)
  :abstract-flavor
  :settable-instance-variables
  :gettable-instance-variables
  :initable-instance-variables)

(defsubst get-state-for-end-of-line (line start-state)
  (let* ((previous-line (zwei::line-previous line))
	 (previous-syntactic-token
	   (when previous-line
	     (grammar::previous-token-from-line previous-line t))))
    (if (and previous-syntactic-token
	     (send previous-syntactic-token :multi-line-syntactic-token-p))
	(send previous-syntactic-token :eol-state)
	start-state)))

(defmacro mid-or-begin-token-terminal-p (token)
  `(or (eq ,token begin-token-terminal)
       (eq ,token mid-token-terminal)))

(defmethod (set-additional-ivs multi-line-tokenizer) (cfg)
  (setq mid-token-terminal	  (resolve-regular-expression-symbol cfg 'mid-token)
	end-token-terminal	  (resolve-regular-expression-symbol cfg 'end-token)))


D,#TD1PsT[Begin using 006 escapes](1 0 (NIL 0) (NIL :ITALIC NIL) "CPTFONTI");; Tokenize a region of lines for the parser.  The extra kruft here is to deal with
;; the fact that Pascal has multi-line comments.  A C-tokenizer would be similar (identical
;; if strings across lines was handled correctly). An Ada-tokenizer would be able to use the
;; (tokenizer-with-cfg :lex-lines) method since it has no tokens which cross line boundaries.
0(defmethod (:lex-lines multi-line-tokenizer)
	   (first-line last-line &optional (parent nil))
  (loop with start-line		= first-line
	with final-line		= last-line
	as line-after		= (zwei:line-next final-line)
	as token-after		= (when line-after (lexer::get-line-token line-after))
1	;; The assumption is that the line beyond the range has a token on it if it was a
	;; multi-line-syntactic-token otherwise its state is the start-state whether or not
	;; is found.
0	as pre-lex-state
	   = (cond
1	       ;; token-after may be null in the rare case where we have two separate
	       ;; ranges of lines in the flush-mung-region-array and :lex-lines bridges
	       ;; that gap.
0	       ((null token-after)		*fsm-start-state*)
1	       ;; empty line
0	       ((eq token-after line-after)	*fsm-start-state*)
	       (t				(send token-after :start-state)))
	as last-token =
	   (tokenize-lines
	     self parent start-line
	     (loop for i upfrom 1
		   for line = start-line then (zwei:line-next line)
		   while line
		   until (eq line final-line)
		   finally (return i)))
1	;; the eol-state of the last token returned from tokenize-lines is the new start state
	;; for token-after
0	as post-lex-state = (if (grammar:token-is-line-p last-token)
				*fsm-start-state* (send last-token :eol-state))
1	;; If the pre- and post- lex-states are not = , then we must continue tokenizing lines0.
	until (= post-lex-state pre-lex-state)
	do (when (setq start-line line-after)
	     (setq final-line  start-line))
	while line-after
	finally (return final-line)))


1;;; Initialize part of the state of the lexer so that it is ready to start lexing a line.
;;; This method must be invoked, before any tokenized state has been initialized, since
;;; get-state-for-end-of-line may need to invoke the lexer recursively (if we are in the
;;; middle of a compound parse tree node).
0(defmethod (initialize-lexer-to-start-of-line multi-line-tokenizer) (line initial-state)
  (setq state 		(or initial-state (get-state-for-end-of-line line start-state))
	buffer 		line
	start 		0
	cursor 		0
	last-match 	0
	skip-token  	nil
	eof-p           nil
	outgoing-state  start-state
	buffer-size     (zwei:line-length line)))


(defmethod (get-fsm-state-for-string multi-line-tokenizer)
	   (string
	     &optional
	     (start-index 0) (end-index (string-length string)) (starting-state start-state))
  (declare (sys:array-register fast-buffer fast-comb-index-array fast-comb))

1  ;; Save away the ivs representing the lexer state that will be modified.
0  (let ((state state)
	(cursor cursor))

    (setq state		   starting-state
	  cursor	   start-index)
    (loop with fast-buffer = string
	  with fast-comb-index-array = grammar::comb-index-array
	  with fast-comb = grammar::comb
	  while (< cursor end-index)
	  as previous-state = state
	  when (null (setq state (next-zmacs-lexer-state
				   state (get-char-from-fsm cursor end-index fast-buffer))))
	    return (setq state previous-state)
	  finally (return state))))


;;;
;;; Useful debugging routines
;;;

(defun print-tokens-from-line (&optional (start-line zwei:(bp-line (point))))
  (format t "~%")
  (loop for line = start-line then (zwei:line-next line) while line
	do (print-tokens-on-line line))
  (values))

(defun print-tokens-on-line (&optional (line zwei:(bp-line (point))))
  (loop for token = (grammar::get-line-token line)
		  then (send token :next)
	while token
	until (or (eq token line)
		  (typep token 'grammar:compound-parse-tree-node))
	for i upfrom 1
	do (format t "~v~D:~ ~\format:presentation\ " '(:fix :bold :small) i token)
	finally (format t "~%")))
