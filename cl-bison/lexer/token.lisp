;;; -*- Mode: LISP; Package: LEXER; Lowercase: T; Base: 10; Syntax: Zetalisp -*-

;;;>
;;;> *****************************************************************************************
;;;> This data and information is proprietary to, and a valuable trade
;;;> secret of, Symbolics, Inc.  It is given in confidence by Symbolics,
;;;> and may only be used as permitted under the license agreement under
;;;> which it has been distributed, and in no other way.
;;;>
;;;> ** Enhancements (c) Copyright 1982, 1983, 1984 by Symbolics, Inc.  All rights reserved.
;;;>
;;;> Symbolics (TM), ZetaLisp (TM), and Macsyma (TM), are trademarks of Symbolics, Inc.
;;;>
;;;> The technical data and information provided herein are provided with
;;;> `limited rights', and the computer software provided herein is provided
;;;> with `restricted rights' as those terms are defined in DAR and ASPR 7-104.9(a).
;;;> *****************************************************************************************

;;; A vanilla token - used by the action code
;;;
(defflavor vanilla-token
	   ((symbol nil))			; The grammar symbol
	   ()
  :initable-instance-variables
  :gettable-instance-variables
  :settable-instance-variables :writable-instance-variables)

;;;
;;; Pass all messages on to the symbol - tied into next-legal-token-message to :parse
;;; This is used if the message is :next-legal-value-token rather than :next-legal-token
;;;
(defmethod (:unclaimed-message vanilla-token) (message &optional arguments)
  (if arguments
      (send (send self :symbol) message arguments)
      (send (send self :symbol) message)))

;;;
;;; used by the action code to associate a value with the symbol
(defflavor value-token
	((value nil))	  ; The value associated with the symbol (usually the source string)
	(vanilla-token)
  :initable-instance-variables
  :settable-instance-variables
  :gettable-instance-variables)

D,#TD1PsT[Begin using 006 escapes](1 0 (NIL 0) (NIL :ITALIC NIL) "CPTFONTI");;;
;;; A tokenizer prototype that defines the interface to the parsers
;;; 
0(defflavor tokenizer
	((lexer)
	 (token (make-instance 'vanilla-token)))
	()
  :initable-instance-variables :settable-instance-variables :gettable-instance-variables)

(defmethod (initialize-tokenizer tokenizer)(stream)
  (reset-lexer lexer stream))

(defmethod (:next-legal-token tokenizer :default)()
    (setf (vanilla-token-symbol token) (next-token lexer))
    (and (vanilla-token-symbol token) token))

(defmethod (:current-left-context tokenizer :default)(&optional (lines 1))
  (send lexer :current-left-context lines))