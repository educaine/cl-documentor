;;; -*- Syntax: Common-lisp; Base: 10; Mode: Lisp; Package: LEXER -*-

D,#TD1PsT[Begin using 006 escapes](1 0 (NIL 0) (NIL :ITALIC NIL) "CPTFONTI");;; This set must be maintained as new operators are created
0(defconst *lexer-operators*
	  '(#\ #\� #\[ #\] #\ #\ #\ #\ #\  #\? #\ #\� #\( #\)
	    #\ #\ #\{ #\} #\return #\ #\))


(defconst *lexer-escape-operator* #\)
(defconstant *fsm-start-state* 0)

;;; Compare the character and nothing else ....
(defsubst ignoring-style-char= (ch1 ch2)
  (zerop (dpb 0 si:%%char-style (logxor (char-int ch1) (char-int ch2)))))

(defun %ignoring-style-string= (string1 index1 string2 index2 count)
  (declare lt:(side-effects reader reducible))
  (cond ((if (null count)
	     ( (setq count (- (array-active-length string1) index1))
		(- (array-active-length string2) index2))
	     (or (> (+ index1 count) (array-active-length string1))
		 (> (+ index2 count) (array-active-length string2))))
	 nil)
	(t (let ((string1 string1) (string2 string2))
	     (declare (sys:array-register string1 string2))
	     (do () ((zerop count) t)
	       (unless (ignoring-style-char= (aref string1 index1) (aref string2 index2))
		 (return nil))
	       (incf index1)
	       (incf index2)
	       (decf count))))))

(cli::define-optimized-keyword-function ignoring-style-string=
	(string1 string2) ((start1 0) (end1 nil) (start2 0) (end2 nil))
  (declare lt:(side-effects reader reducible))
  (cli::coerce-string-arg string1)
  (cli::coerce-string-arg string2)
  (cond ((or end1 end2)
	 (unless end1 (setq end1 (zl:array-active-length string1)))
	 (unless end2 (setq end2 (zl:array-active-length string2)))
	 (and (= (setq end1 (- end1 start1)) (- end2 start2))
	      (%ignoring-style-string= string1 start1 string2 start2 end1)))
	(t (%ignoring-style-string= string1 start1 string2 start2 nil))))

