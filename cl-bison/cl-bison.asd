
(asdf:defsystem :cl-bison
  :depends-on (:lispbuilder-lexer :lispbuilder-yacc :iterate)
  :components
  ((:file "package")
   (:module clex
	    :components
	    ((:file "lexer")))
   (:file "functions")
   (:file "cl-bison")
   ;;(:file "clex-lab")
   ))
