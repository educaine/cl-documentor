
(asdf:defsystem :cl-documenter
  :depends-on (:iterate :dso-lex :cl-ppcre :org.xoanonos.core :fiveam :cl-lex :cl-lexer)
  :components
  ((:file "package")
   (:file "main")
   (:file "scribetext/parse")
   (:file "scribetext/scribetext-function")
   (:file "scribetext/main")
   (:file "scribetext/parse-test")))
