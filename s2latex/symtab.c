// symtab.c       1.2    85/02/04
// symbol table routines for scribe-to-latex
//
//  copyright (c) 1984 by Van Jacobson, Lawrence Berkeley Laboratory
//  This program may be freely redistributed but not for profit.  This
//  comment must remain in the program or any derivative.

#include <ctype.h>
#include "symtab.h"
#include <string.h>

#define HASHSIZE 127

// max char in a symbol name
#define MAXSYM        128
#define SYMHASH(str) ((str[0] + (str[1]<<8)) % HASHSIZE)

static struct stab* sthash[HASHSIZE];

void lc_strcpy(char* dst, char* src);

struct stab* lookup(char* str) {
  struct stab* s;
  char text[MAXSYM];
  char* cp = text;
  char* textend = &text[MAXSYM - 1];
  // convert the string to lower case, then try to find it
  while(*str && cp < textend) {
    *(cp++) = (isupper(*str) ? tolower(*str++) : *str++);
  }
  *cp = '\0';
  s = sthash[SYMHASH(text)];
  while(s && strcmp(text, s->s_text)) {
    s = s->s_next;
  }
  return s;
}

struct stab* enter(char* text, int type, char* reptext) {
  register struct stab *n;
  // set up the new entry
  n = (struct stab *) malloc( sizeof(struct stab) );
  n->s_text = (char *) malloc( strlen(text)+1 );
  lc_strcpy( n->s_text, text );
  n->s_reptext = (char *) malloc( strlen(reptext)+1 );
  lc_strcpy( n->s_reptext, reptext );
  n->s_type = type;
  // add it to the table
  n->s_next = sthash[SYMHASH(n->s_text)];
  sthash[SYMHASH(n->s_text)] = n;
  return(n);
}

// Copy a string converting upper case to lower case.
void lc_strcpy(char* dst,char* src) {
  while(*src) {
    *dst++ = isupper(*src) ? tolower(*src++): *src++;
  }
  *dst = '\0';
}
