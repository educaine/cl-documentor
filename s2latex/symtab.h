// symtab.h       1.1    85/02/04

struct stab
{
  struct stab* s_next;
  int s_type;
  char* s_text;
  char* s_reptext;
};

struct stab* lookup(char* str);
struct stab* enter(char* text, int type, char* reptext);
