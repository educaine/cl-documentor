
(in-package :cl-documenter)

;; (deflexer scribe-lexer
;;     ("@[:alpha:][:alnum:]*"
;;      (return (values 'flt (num %0))))
;;     ;; ("@[0-9]+([.][0-9]+([Ee][0-9]+)?)"
;;     ;;   (return (values 'flt (num %0))))
;;     ("[0-9]+"
;;       (return (values 'int (int %0))))
;;     ("[:alpha:][:alnum:]*"
;;       (return (values 'name %0)))
;;     ("[:space:]+") )

;; (lispbuilder-lexer::deflexer test-lexer
;;     ("[0-9]+([.][0-9]+([Ee][0-9]+)?)"
;;       (return (values 'flt (num %0))))
;;     ("[0-9]+"
;;       (return (values 'int (int %0))))
;;     ("[:alpha:][:alnum:]*"
;;       (return (values 'name %0)))
;;     ("[:space:]+") )


(defparameter *tokenization* '())
(defparameter *my-stream* (make-string-input-stream "@aaaa"))

(defun snip (s) (subseq s 1 (1- (length s))))

(defun un-squote (s) (cl-ppcre::regex-replace-all "''" (snip s) "'"))

(defun un-dquote (s) (cl-ppcre::regex-replace-all "\"\"" (snip s) "\""))

(defun print-me (s)
  (let* ((token-name (subseq s 1))
	 (token (intern  (string-upcase token-name) :keyword)))
    (format t "print me~s~%" token)
    token))

;; (dso-lex::deflexer scan-csv (:priority-only t)
;;   ("@\\w+"  directive print-me))
;; ("," comma)
;; ("[^\"',]+" value)
;; ("'(?:[^']|'')*'" value un-squote)
;; ("\"(?:[^\"]|\"\")*\"" value un-dquote))

;;(dso-lex::lex-all 'scan-csv "no quotes,'a ''quote''',\"another \"\"quote\"\"\"")

(defun tokenize ()
  (setf *tokenization* '())
  (push (dso-lex::lex-all 'scan-csv "@aaa")
	*tokenization*))


(defun parse-scribe-file ()
  (do ((ch (read-char *my-stream* nil :eof)
	   (read-char *my-stream* nil :eof)))
      ((eq ch :eof))))
