(in-package :cl-documenter)

(defparameter *file-in* nil)

(defun set-file-in (file-name)
  (when *file-in*
    (close *file-in*))
  (setf *file-in* (open file-name :direction :input)))

(defun list-to-string (l)
  (map 'string #'identity l))

(defun get-instruction ()
  (map 'string #'identity
       (iter (for ch = (read-char *file-in* nil :eof))
	     (when (member ch
			   collect next)))))

(defun get-command-result ()
  "We've encountered an @ character, and need to precess the
   possibleresults: 1) literal @ character, e.g. #(#\@ #\@); 2 literal
   whitespace (possibly to be normalized), e.g. #(#\@ #\Space); 3)
   character command, e.g. @^ (superscript); 4) command, e.g. @bold"
  (let ((ch (read-char *file-in* nil :eof)))
    (cond
      ((eq ch #\@)
       (list :text #\@))
      ((eq ch #\Space)
       (list :text #\Space))
      ((member ch *scribechars*)   ; escape a space)
       (list :text (execute-special ch)))
      (t
       (error t "Unexpected character following '@'")))))

(defparameter *scribe-white-space* '(#\Space #\Tab))

;; (union (union *scribe-white-space* *scribe-open-delimiters*)
;;        *scribe-close-delimiters*)


(defun read-command ()
  (let* ((possible-delimiters
	  (union
	   (union *scribe-white-space* *scribe-open-delimiters*)
	   *scribe-close-delimiters*))
	 (command '()))
    (push :eof possible-delimiters)
    (iter
      (for ch = (read-char *file-in* nil :eof))
      (when (member ch possible-delimiters)
	(return  (list :command (list-to-string (reverse command)))))
      (push ch command))))

(defun find-node (key)
  (gethash key *syntax-table*))

(defun execute-command (&key command)
  (let* ((command (string-downcase command))
	 (node (find-node (string-downcase command))))
    (cond ((null node)
	   (format t "unable to find command: ~s in syntax table" command))
	  (t '()))
    (if node

	node)))

(defun get-execute-command ()
  (iter
    (for ch = (read-char *file-in* nil :eof))
    (cond
      ((eq ch #\@) (return '(:text :ampersand)))
      ((eq ch #\Space) (return '(:text :space)))
      ((eq ch #\Tab) (return '(:text :tab)))
      ((eq ch :eof) '())
      (t
       (unread-char ch *file-in*)
       (return (apply #'execute-command (read-command)))))))



;; lexical syntax table

;; desicion of what to be returned can be decide on by looking at the
;; current character, one character ahead and knowing tho current
;; context.  The result tells us whether to return the read character,
;; return an exscaped character or return the evaluation of a
;; function, and it tells us whether to put back the read ahead
;; character or not.


;;({[<




(defparameter *scribechars*
  '(#\! #\$ #\* #\. #\: #\/ #\= #\> #\\ #\| #\^ #\& #\) #\; #\~))

(defparameter *scribe-open-delimiters* '(#\` #\' #\( #\{ #\[ #\<))
(defparameter *scribe-close-delimiters* '(#\' #\) #\} #\] #\>))



;; For character classes we are only interested in CL standard-char
(defun char-class-all (ch)
  "Match everything except nil."
  (standard-character-p ch))

(defun char-class-word)
  "Upper and Lowercas characters numbers and the underscore."
  (or (alphanumericp ch)
      (eq ch #\_)))

(defparameter *lexical-table*
  '((:ch #\@ :next-ch #\@ :context :all :return
     (:return-type :self :consume-next-ch t))
    (:ch #\@ :next-ch :word :context :text :return
     (:return-type 'execute-command :consume-next-ch nil))
    (:ch #\) :next-ch :all :context :within-brackets :return
     (:return-type :close-delimiter :consume-next-ch t))
    (:ch :all :next-ch :all :context '(:text :within-brackets) :return
     (:return-type :self :consume-next-ch nil))
    ))

(defun lexical-type-matches-p (lookup-ch ch-match-patern)
  "The following ar valid paterns: a character, e.g. #\a, a character
class, i.e. a function that returns true or false or a list of other
character classes or other characters or other lists classes"
  (cond
    ((and (typep ch-match-patern 'character)
	  (eq lookup-ch ch-match-patern))
     lookup-ch)
    ((and (typep ch-match-patern 'symbol)
	  (funcall ch-match-patern lookup-ch))
     lookup-ch)
    (t
     nil)))

(defun get-lexical-syntax-entry (lookup-ch lookup-next-ch lookup-context)
  (dolist (entry *lexical-table*)
    (destructuring-bind (&key ch next-ch context return)
	entry
      (when (and (or (eq context :all)
		     (eq context lookup-context))
		 (lexical-type-matches-p lookup-ch ch)
		 (lexical-type-matches-p lookup-next-ch next-ch))
	(return return)))))

(defun within-brackets-paren ()
  (let ((text ""))
    (iter
      (for ch = (or next-ch
		    (read-char *file-in* nil :eof)))
      (for next-ch = (read-char *file-in* nil :eof))
      (progn
	(format t "chars form file: ch: ~s; next-ch: ~s~%" ch next-ch)
	(let ((result (get-lexical-syntax-entry ch next-ch :within-brackets-paren)))
	  (if result
	      (destructuring-bind (&key return-type unread) result
		(format t "lexical table return: ~s~%" result)
		(cond ((eq return-type :close-delimiter)
		       (unread-char next-ch *file-in*)
		       (return text))
		      ((eq return-type :self)
		       (setf text (concatenate 'string text (string ch))))
		      ((symbolp return-type)
		       (funcall return-type))
		      ((eq ch :eof)
		       (format
			t
			":eof found before closing delimiter. Exitting..."))
		      (t
		       (error
			t
			"Unexpected error return type from lexical table: ~s~%"
			return-type))))
	      (setf text (concatenate 'string text (string ch)))))))))

(defun run-within-brackets-paren ()
  (set-file-in "/home/rett/dev/garnet/cl-documenter/scribetext/test-within-brackets-paren.mss")
  (within-brackets-paren))


	;;  (or (eq ch to-find)
	;; 	(eq ch :eof)
	;; 	(and (eq ch #\@)))
	;; (cond
	;;   ((and (eq ch :eof)
	;; 	(not (eq ch to-find)))
	;;    (format t "Uexpected end of file.  Continuing processing..."))
	;;   ((eq ch #\@)
	;;    (let ((next-char
	;; 	  (return text))
	;; 	 (if (eq ch #\@)
	;; 	     (format t "~s~%" (get-execute-command))
	;; 	     (setf text (concatenate 'string text (string ch))))))))))))

(defun get-text (to-find)
  (let (text)
    (iter
      (for ch = (or next-ch
		    (read-char *file-in* nil :eof)))
      (for next-ch = (read-char *file-in* nil :eof))
      (when (or (eq ch to-find)
		(eq ch :eof)
		(and (eq ch #\@)))
	(cond
	  ((and (eq ch :eof)
		(not (eq ch to-find)))
	   (format t "Uexpected end of file.  Continuing processing..."))
	  ((eq ch #\@)
	   (let ((next-char
		  (return text))
		 (if (eq ch #\@)
		     (format t "~s~%" (get-execute-command))
		     (setf text (concatenate 'string text (string ch))))))))))))

;; (defun get-next-lexical-item (read-context &key (bracket-type nil))
;;   "valid contexts: :text :command :within-brackets"
;;   (let ((ch (read-char *file-in* nil :eof))
;; 	(next-ch (read-char *file-in* nil :eof)))
;;     (cond
;;       ((and (eq read-context :within-brackets)
;; 	    (
;; 	     ((and (eq read-context :text)
;; 		   (not (eq ch #\@)))
;; 	      ch)
;; 	     ((and (eq read-context :text)
;; 		   (eq ch #\@)
;; 		   (eq next-ch #\@))
;; 	      #\@)))))))



(defun parse-text (item-type)
  (let ((document '())
	(content '(:type :node))
	(properties '(:properties '()))
	(text-property '(:text))
	(text nil))
    (setf text-property
	  `(:type :node
		  :name "root"
		  :children
		  ((:type :node
			  :children '()
			  :properties ((:text
					,a))))))
    text-property))
  ;; (push text-property (cdr properties))
  ;; (push properties content)
  ;; (push content document))))

;;  collect next)



(defun run-execute-command ()
  (set-file-in "/home/rett/dev/garnet/cl-documenter/scribetext/test-execute-command.mss")
  (apply #'execute-command  (read-command)))

(defun run-read-command ()
  (set-file-in "/home/rett/dev/garnet/cl-documenter/scribetext/test-read-command.mss")
  (read-command))

(defun run-parse-text ()
  (set-file-in "/home/rett/dev/garnet/cl-documenter/scribetext/test-parse-text.mss")
  (parse-text))


;;  (read-rest-instruction-from-stream *file-in*))
;; (parse-text))





;; (defrule list (and #\( sexp (* sexp) (? whitespace) #\))
;;   (:destructure (p1 car cdr w p2)
;;     (declare (ignore p1 p2 w))
;;     (cons car cdr)))
