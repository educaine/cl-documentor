(in-package :cl-documenter)


(defparameter *tests-assertions-lexical-type-matches-p*
  '((#\@ #\@)))



(fiveam:test run-lexical-type-matches-p
  "test different types of characters."
  ;; Literal character
  (fiveam:is (lexical-type-matches-p #\@ #\@))
  (fiveam:is (not (lexical-type-matches-p #\1 #\@)))
  ;; Character class
  (fiveam:is (lexical-type-matches-p #\a 'ch-class-word-p))
  (fiveam:is (lexical-type-matches-p #\A 'ch-class-word-p))
  (fiveam:is (lexical-type-matches-p #\1 'ch-class-word-p))
  (fiveam:is (lexical-type-matches-p #\_ 'ch-class-word-p))
  (fiveam:is (not (lexical-type-matches-p #\$ 'ch-class-word-p))))

(fiveam:test run-get-lexical-syntax-entry
  (fiveam:is (get-lexical-syntax-entry #\@ #\@ :all))
  (fiveam:is-false (get-lexical-syntax-entry #\r #\t :within-brackets-paren)))
