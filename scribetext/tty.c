#include <sys/types.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/stat.h>
# include <sys/file.h>
# include <sys/ioctl.h> /* collosions with termios.h */
#include <limits.h>
#include "config.h"
#include <sys/stropts.h>	/* for I_POP */

#include "screen.h"
#include "extern.h"

extern struct display *display, *displays;
extern int iflag;

extern struct win *console_window;
static void consredir_readev_fn (struct event *, char *);

int separate_sids = 1;

static void DoSendBreak __P((int, int, int));
static sigret_t SigAlrmDummy __P(SIGPROTOARG);

/* Frank Schulz (fschulz@pyramid.com):
 * I have no idea why VSTART is not defined and my fix is probably not
 * the cleanest, but it works.
 */

static sigret_t
SigAlrmDummy SIGDEFARG
{
  debug("SigAlrmDummy()\n");
  SIGRETURN;
}

/*
 *  Carefully open a charcter device. Not used to open display ttys.
 *  The second parameter is parsed for a few stty style options.
 */

int OpenTTY(char *line, *optopt) {
  int f;
  struct mode Mode;
  sigret_t (*sigalrm)__P(SIGPROTOARG);
  sigalrm = signal(SIGALRM, SigAlrmDummy);
  alarm(2);
  /* this open only succeeds, if real uid is allowed */
  if ((f = secopen(line, O_RDWR | O_NONBLOCK | O_NOCTTY, 0)) == -1) {
    if (errno == EINTR) {
      Msg(0, "Cannot open line '%s' for R/W: open() blocked, aborted.", line);
    } else {
      Msg(errno, "Cannot open line '%s' for R/W", line);
    }
    alarm(0);
    signal(SIGALRM, sigalrm);
    return -1;
  }
  if (!isatty(f)) {
    Msg(0, "'%s' is not a tty", line);
    alarm(0);
    signal(SIGALRM, sigalrm);
    close(f);
    return -1;
  }
  debug("OpenTTY I_POP\n");
  while (ioctl(f, I_POP, (char *)0) >= 0) {
    ;
  }
  /* We come here exclusively. This is to stop all kermit and cu type things
   * accessing the same tty line.
   * Perhaps we should better create a lock in some /usr/spool/locks directory? */
  errno = 0;
  if (ioctl(f, TIOCEXCL, (char *) 0) < 0) {
    Msg(errno, "%s: ioctl TIOCEXCL failed", line);
  }
  debug3("%d %d %d\n", getuid(), geteuid(), getpid());
  debug2("%s TIOCEXCL errno %d\n", line, errno);
  InitTTY(&Mode, W_TYPE_PLAIN);
  SttyMode(&Mode, opt);
  SetTTY(f, &Mode);
  int mcs = 0;
  ioctl(f, TIOCMGET, &mcs);
  mcs |= TIOCM_RTS;
  ioctl(f, TIOCMSET, &mcs);
  brktty(f);
  alarm(0);
  signal(SIGALRM, sigalrm);
  debug2("'%s' CONNECT fd=%d.\n", line, f);
  return f;
}

/*  Tty mode handling */
void InitTTY(struct mode *m, intttyflag) {
  bzero((char *)m, sizeof(*m));
  debug1("InitTTY: POSIX: termios defaults based on SunOS 4.1.3, but better (%d)\n",
	 ttyflag);
  m->tio.c_iflag |= BRKINT;
  m->tio.c_iflag |= IGNPAR;
  m->tio.c_iflag |= IXON;
  if (!ttyflag)	/* may not even be good for ptys.. */ {
    m->tio.c_iflag |= ICRNL;
    m->tio.c_oflag |= ONLCR;
    m->tio.c_oflag |= TAB3;
    m->tio.c_oflag |= OXTABS;
    m->tio.c_oflag |= OPOST;
  }
  cfsetospeed(&m->tio, B9600);
  cfsetispeed(&m->tio, B9600);
  m->tio.c_cflag |= CS8;
  m->tio.c_cflag |= CREAD;
  m->tio.c_cflag |= CLOCAL;
  m->tio.c_lflag |= ECHOCTL;
  m->tio.c_lflag |= ECHOKE;
  if (!ttyflag) {
    m->tio.c_lflag |= ISIG;
    m->tio.c_lflag |= ICANON;
    m->tio.c_lflag |= ECHO;
  }
  m->tio.c_lflag |= ECHOE;
  m->tio.c_lflag |= ECHOK;
  m->tio.c_lflag |= IEXTEN;
  m->tio.c_cc[VINTR]    = Ctrl('C');
  m->tio.c_cc[VQUIT]    = Ctrl('\\');
  m->tio.c_cc[VERASE]   = 0x7f; /* DEL */
  m->tio.c_cc[VKILL]    = Ctrl('H');
  m->tio.c_cc[VEOF]     = Ctrl('D');
  m->tio.c_cc[VEOL]     = 0000;
  m->tio.c_cc[VEOL2]    = 0000;
  m->tio.c_cc[VSWTCH]   = 0000;
  m->tio.c_cc[VSTART]   = Ctrl('Q');
  m->tio.c_cc[VSTOP]    = Ctrl('S');
  m->tio.c_cc[VSUSP]    = Ctrl('Z');
  m->tio.c_cc[VDSUSP]   = Ctrl('Y');
  m->tio.c_cc[VREPRINT] = Ctrl('R');
  m->tio.c_cc[VDISCARD] = Ctrl('O');
  m->tio.c_cc[VWERASE]  = Ctrl('W');
  m->tio.c_cc[VLNEXT]   = Ctrl('V');
  m->tio.c_cc[VSTATUS]  = Ctrl('T');
  if (ttyflag) {
    m->tio.c_cc[VMIN] = TTYVMIN;
    m->tio.c_cc[VTIME] = TTYVTIME;
  }
  m->m_jtchars.t_ascii = 'J';
  m->m_jtchars.t_kanji = 'B';
  m->m_knjmode = KM_ASCII | KM_SYSSJIS;
}

void SetTTY(int fd, struct mode *mp) {
  errno = 0;
  tcsetattr(fd, TCSADRAIN, &mp->tio);
  ioctl(fd, TIOCKSETC, &mp->m_jtchars);
  ioctl(fd, TIOCKSET, &mp->m_knjmode);
  if (errno) {
    Msg(errno, "SetTTY (fd %d): ioctl failed", fd);
  }
}

void GetTTY(int fd, struct mode *mp) {
  errno = 0;
  ioctl(fd, TIOCKGETC, &mp->m_jtchars);
  ioctl(fd, TIOCKGET, &mp->m_knjmode);
  if (errno) {
    Msg(errno, "GetTTY (fd %d): ioctl failed", fd);
  }
}

/* needs interrupt = iflag and flow = d->d_flow */
void SetMode(struct mode *op, struct mode *np, int flow, int interrupt) {
  *np = *op;
  ASSERT(display);
  np->m_mapkey = NOMAPKEY;
  np->m_mapscreen = NOMAPSCREEN;
  np->tio.c_line = 0;
  np->tio.c_iflag &= ~ICRNL;
  np->tio.c_iflag &= ~ISTRIP;
  np->tio.c_oflag &= ~ONLCR;
  np->tio.c_lflag &= ~(ICANON | ECHO);
  /* From Andrew Myers (andru@tonic.lcs.mit.edu)
   * to avoid ^V^V-Problem on OSF1 */
  np->tio.c_lflag &= ~IEXTEN;
  /* Unfortunately, the master process never will get SIGINT if the real
   * terminal is different from the one on which it was originaly started
   * (process group membership has not been restored or the new tty could not
   * be made controlling again). In my solution, it is the attacher who
   * receives SIGINT (because it is always correctly associated with the real
   * tty) and forwards it to the master [kill(MasterPid, SIGINT)].
   * Marc Boucher (marc@CAM.ORG) */
  if (interrupt) {
    np->tio.c_lflag |= ISIG;
  } else {
    np->tio.c_lflag &= ~ISIG;
  }
  /* careful, careful catche monkey..
   * never set VMIN and VTIME to zero, if you want blocking io.
   * We may want to do a VMIN > 0, VTIME > 0 read on the ptys too, to
   * reduce interrupt frequency.  But then we would not know how to
   * handle read returning 0. jw. */
  np->tio.c_cc[VMIN] = 1;
  np->tio.c_cc[VTIME] = 0;
  if (!interrupt || !flow) {
    np->tio.c_cc[VINTR] = VDISABLE;
  }
  np->tio.c_cc[VQUIT] = VDISABLE;
  if (flow == 0) {
    np->tio.c_cc[VSTART] = VDISABLE;
    np->tio.c_cc[VSTOP] = VDISABLE;
    np->tio.c_iflag &= ~IXON;
  }
  np->tio.c_cc[VDISCARD] = VDISABLE;
  np->tio.c_cc[VLNEXT] = VDISABLE;
  np->tio.c_cc[VSTATUS] = VDISABLE;
  np->tio.c_cc[VSUSP] = VDISABLE;
  /* Set VERASE to DEL, rather than VDISABLE, to avoid libvte
     "autodetect" issues. */
  np->tio.c_cc[VERASE] = 0x7f;
  np->tio.c_cc[VKILL] = VDISABLE;
  np->tio.c_cc[VDSUSP] = VDISABLE;
  np->tio.c_cc[VREPRINT] = VDISABLE;
  np->tio.c_cc[VWERASE] = VDISABLE;
}

/* operates on display */
void SetFlow(int on) {
  ASSERT(display);
  if (D_flow == on) {
    return;
  }
  if (on) {
    D_NewMode.tio.c_cc[VINTR] = iflag ? D_OldMode.tio.c_cc[VINTR] : VDISABLE;
    D_NewMode.tio.c_cc[VSTART] = D_OldMode.tio.c_cc[VSTART];
    D_NewMode.tio.c_cc[VSTOP] = D_OldMode.tio.c_cc[VSTOP];
    D_NewMode.tio.c_iflag |= D_OldMode.tio.c_iflag & IXON;
  } else {
    D_NewMode.tio.c_cc[VINTR] = VDISABLE;
    D_NewMode.tio.c_cc[VSTART] = VDISABLE;
    D_NewMode.tio.c_cc[VSTOP] = VDISABLE;
    D_NewMode.tio.c_iflag &= ~IXON;
  }
  if (!on) {
    tcflow(D_userfd, TCOON);
  }
  if (tcsetattr(D_userfd, TCSANOW, &D_NewMode.tio))
    debug1("SetFlow: ioctl errno %d\n", errno);
  D_flow = on;
}

/* parse commands from opt and modify m */
int SttyMode(struct mode *m, char *opt) {
  static const char sep[] = " \t:;,";
  if (!opt) {
    return 0;
  }
  while (*opt) {
    while (index(sep, *opt)) {
      opt++;
    }
    if (*opt >= '0' && *opt <= '9') {
      if (SetBaud(m, atoi(opt), atoi(opt)))
	return -1;
    } else if (!strncmp("cs7", opt, 3)) {
      m->tio.c_cflag &= ~CSIZE;
      m->tio.c_cflag |= CS7;
    } else if (!strncmp("cs8", opt, 3)) {
      m->tio.c_cflag &= ~CSIZE;
      m->tio.c_cflag |= CS8;
    } else if (!strncmp("istrip", opt, 6)) {
      m->tio.c_iflag |= ISTRIP;
    } else if (!strncmp("-istrip", opt, 7)) {
      m->tio.c_iflag &= ~ISTRIP;
    } else if (!strncmp("ixon", opt, 4)) {
      m->tio.c_iflag |= IXON;
    } else if (!strncmp("-ixon", opt, 5)) {
      m->tio.c_iflag &= ~IXON;
    } else if (!strncmp("ixoff", opt, 5)) {
      m->tio.c_iflag |= IXOFF;
    } else if (!strncmp("-ixoff", opt, 6)) {
      m->tio.c_iflag &= ~IXOFF;
    } else if (!strncmp("crtscts", opt, 7)) {
      m->tio.c_cflag |= CRTSCTS;
    } else if (!strncmp("-crtscts", opt, 8)) {
      m->tio.c_cflag &= ~CRTSCTS;
    } else {
      return -1;
    }
    while (*opt && !index(sep, *opt)) {
      opt++;
    }
  }
  return 0;
}

/*  Job control handling
 *
 *  Somehow the ultrix session handling is broken, so use
 *  the bsdish variant. */

/*ARGSUSED*/
void brktty(int fd) {
  // Will break terminal affiliation
  if (separate_sids) {
    setsid();
  }
}

int fgtty(int fd) {
  int mypid;
  mypid = getpid();
  // Under BSD we have to set the controlling terminal again explicitly.
  ioctl(fd, TIOCSCTTY, (char *)0);
  if (separate_sids)
    if (tcsetpgrp(fd, mypid)) {
      debug1("fgtty: tcsetpgrp: %d\n", errno);
      return -1;
    }
  return 0;
}

/* The alm boards on our sparc center 1000 have a lousy driver.
 * We cannot generate long breaks unless we use the most ugly form
 * of ioctls. jw. */
int breaktype = 2;

/* type:
 *  0:	TIOCSBRK / TIOCCBRK
 *  1:	TCSBRK
 *  2:	tcsendbreak()
 *  n: approximate duration in 1/4 seconds. */
static void DoSendBreak(int fd, int n, int type) {
  int i;
  switch (type) {
  case 2:
    /* There is one rare case that I have tested, where tcsendbreak
     * works really great: this was an alm driver that came with SunOS
     * 4.1.3 If you have this one, define the above symbol.  here we
     * can use the second parameter to specify the duration. */
    debug2("tcsendbreak(fd=%d, %d)\n", fd, n);
    if (tcsendbreak(fd, n) < 0) {
      Msg(errno, "cannot send BREAK (tcsendbreak)");
    }
    break;
  case 1:
    if (!n) {
      n++;
    }
    /* Here too, we assume that short breaks can be concatenated to
     * perform long breaks. But for SOLARIS, this is not true, of
     * course. */
    debug2("%d * TCSBRK fd=%d\n", n, fd);
    for (i = 0; i < n; i++)
      if (ioctl(fd, TCSBRK, (char *)0) < 0) {
	Msg(errno, "Cannot send BREAK (TCSBRK)");
	return;
      }
    break;
  case 0:
    /* This is very rude. Screen actively celebrates the break.  But
     * it may be the only save way to issue long breaks. */
    debug("TIOCSBRK TIOCCBRK\n");
    if (ioctl(fd, TIOCSBRK, (char *)0) < 0) {
      Msg(errno, "Can't send BREAK (TIOCSBRK)");
      return;
    }
    sleep1000(n ? n * 250 : 250);
    if (ioctl(fd, TIOCCBRK, (char *)0) < 0) {
      Msg(errno, "BREAK stuck!!! -- HELP! (TIOCCBRK)");
      return;
    }
    break;
  default:
    Msg(0, "Internal SendBreak error: method %d unknown", type);
  }
}

/* Send a break for n * 0.25 seconds. Tty must be PLAIN.  The longest
 * possible break allowed here is 15 seconds. */
void SendBreak(struct win *wp, int n, int closeopen) {
  sigret_t (*sigalrm)__P(SIGPROTOARG);
  if (wp->w_type != W_TYPE_PLAIN) {
    return;
  }
  debug3("break(%d, %d) fd %d\n", n, closeopen, wp->w_ptyfd);
  (void) tcflush(wp->w_ptyfd, TCIOFLUSH);
  if (closeopen) {
    close(wp->w_ptyfd);
    sleep1000(n ? n * 250 : 250);
    if ((wp->w_ptyfd = OpenTTY(wp->w_tty, wp->w_cmdargs[1])) < 1) {
      Msg(0, "Ouch, cannot reopen line %s, please try harder", wp->w_tty);
      return;
    }
    (void) fcntl(wp->w_ptyfd, F_SETFL, FNBLOCK);
  } else {
    sigalrm = signal(SIGALRM, SigAlrmDummy);
    alarm(15);
    DoSendBreak(wp->w_ptyfd, n, breaktype);
    alarm(0);
    signal(SIGALRM, sigalrm);
  }
  debug(" broken.\n");
}

//  Console grabbing

static struct event consredir_ev;
static int consredirfd[2] = {-1, -1};

static void consredir_readev_fn(struct event *ev, char *data) {
  char *p, *n, buf[256];
  int l;
  if (!console_window || (l = read(consredirfd[0], buf, sizeof(buf))) <= 0) {
    close(consredirfd[0]);
    close(consredirfd[1]);
    consredirfd[0] = consredirfd[1] = -1;
    evdeq(ev);
    return;
  }
  for (p = n = buf; l > 0; n++, l--) {
    if (*n == '\n') {
      if (n > p) {
	WriteString(console_window, p, n - p);
      }
      WriteString(console_window, "\r\n", 2);
      p = n + 1;
    }
  }
  if (n > p) {
    WriteString(console_window, p, n - p);
  }
}

/*ARGSUSED*/
int TtyGrabConsole(int fd, int on, char *rc_name) {
  struct display *d;
  int cfd;
  if (on > 0) {
    if (displays == 0) {
      Msg(0, "I need a display");
      return -1;
    }
    for (d = displays; d; d = d->d_next) {
      if (strcmp(d->d_usertty, "/dev/console") == 0) {
	break;
      }
    }
    if (d) {
      Msg(0, "too dangerous - screen is running on /dev/console");
      return -1;
    }
  }
  if (consredirfd[0] >= 0) {
    evdeq(&consredir_ev);
    close(consredirfd[0]);
    close(consredirfd[1]);
    consredirfd[0] = consredirfd[1] = -1;
  }
  if (on <= 0) {
    return 0;
  }
  if ((cfd = secopen("/dev/console", O_RDWR|O_NOCTTY, 0)) == -1) {
    Msg(errno, "/dev/console");
    return -1;
  }
  if (pipe(consredirfd)) {
    Msg(errno, "pipe");
    close(cfd);
    consredirfd[0] = consredirfd[1] = -1;
    return -1;
  }
  if (ioctl(cfd, SRIOCSREDIR, consredirfd[1])) {
    Msg(errno, "SRIOCSREDIR ioctl");
    close(cfd);
    close(consredirfd[0]);
    close(consredirfd[1]);
    consredirfd[0] = consredirfd[1] = -1;
    return -1;
  }
  close(cfd);
  consredir_ev.fd = consredirfd[0];
  consredir_ev.type = EV_READ;
  consredir_ev.handler = consredir_readev_fn;
  evenq(&consredir_ev);
  return 0;
}

/* Read modem control lines of a physical tty and write them to buf
 * in a readable format.
 * Will not write more than 256 characters to buf.
 * Returns buf; */
char * TtyGetModemStatus(int fd, char *buf) {
  char *p = buf;
  unsigned int softcar;
  unsigned int mflags;
  struct mode mtio;	/* screen.h */
  int rtscts;
  int clocal;
  GetTTY(fd, &mtio);
  clocal = 0;
  if (mtio.tio.c_cflag & CLOCAL) {
      clocal = 1;
      *p++ = '{';
    }
  if (!(mtio.tio.c_cflag & CRTSCTS)) {
      rtscts = 0;
    } else {
      rtscts = 1;
    }
  if (ioctl(fd, TIOCGSOFTCAR, (char *)&softcar) < 0) {
    softcar = 0;
  }
  if (ioctl(fd, TIOCMGET, (char *)&mflags) < 0) {
      sprintf(p, "NO-TTY? %s", softcar ? "(CD)" : "CD");
      p += strlen(p);
    } else {
      char *s;
      if (!(mflags & TIOCM_LE))
        for (s = "!LE "; *s; *p++ = *s++) {
	  ;
	}
      s = "!RTS ";
      if (mflags & TIOCM_RTS) {
	s++;
      }
      while (*s) {
	*p++ = *s++;
      }
      s = "!CTS ";
      if (!rtscts) {
          *p++ = '(';
          s = "!CTS) ";
	}
      if (mflags & TIOCM_CTS) {
	s++;
      }
      while (*s) {
	*p++ = *s++;
      }

      s = "!DTR "; if (mflags & TIOCM_DTR) s++;
      while (*s) *p++ = *s++;
      s = "!DSR "; if (mflags & TIOCM_DSR) s++;
      while (*s) *p++ = *s++;
      s = "!CD ";
      if (softcar)
	 {
	  *p++ = '(';
	  s = "!CD) ";
	 }
      if (mflags & TIOCM_CD) s++;
      while (*s) *p++ = *s++;
      if (mflags & TIOCM_RI)
	for (s = "RI "; *s; *p++ = *s++);
      s = "!ST "; if (mflags & TIOCM_ST) s++;
      while (*s) *p++ = *s++;
      s = "!SR "; if (mflags & TIOCM_SR) s++;
      while (*s) *p++ = *s++;
      if (p > buf && p[-1] == ' ')
        p--;
      *p = '\0';
    }
  if (clocal)
    *p++ = '}';
  *p = '\0';
  return buf;
}

/*
 * Old bsd-ish machines may not have any of the baudrate B... symbols.
 * We hope to detect them here, so that the btable[] below always has
 * many entries.
 */
#ifndef POSIX
# ifndef TERMIO
#  if !defined(B9600) && !defined(B2400) && !defined(B1200) && !defined(B300)
IFN{B0}#define		B0	0
IFN{B50}#define  	B50	1
IFN{B75}#define  	B75	2
IFN{B110}#define 	B110	3
IFN{B134}#define 	B134	4
IFN{B150}#define 	B150	5
IFN{B200}#define 	B200	6
IFN{B300}#define 	B300	7
IFN{B600}#define 	B600	8
IFN{B1200}#define	B1200	9
IFN{B1800}#define	B1800	10
IFN{B2400}#define	B2400	11
IFN{B4800}#define	B4800	12
IFN{B9600}#define	B9600	13
IFN{EXTA}#define	EXTA	14
IFN{EXTB}#define	EXTB	15
#  endif
# endif
#endif

/*
 * On hpux, idx and sym will be different.
 * Rumor has it that, we need idx in D_dospeed to make tputs
 * padding correct.
 * Frequently used entries come first.
 */
static struct baud_values btable[] =
{
IF{B9600}	{	13,	9600,	B9600	},
IF{B19200}	{	14,	19200,	B19200	},
IF{EXTA}	{	14,	19200,	EXTA	},
IF{B38400}	{	15,	38400,	B38400	},
IF{EXTB}	{	15,	38400,	EXTB	},
IF{B57600}	{	16,	57600,	B57600	},
IF{B115200}	{	17,	115200,	B115200	},
IF{B230400}	{	18,	230400,	B230400	},
IF{B460800}	{	19,	460800,	B460800	},
IF{B7200}	{	13,	7200,	B7200	},
IF{B4800}	{	12,	4800,	B4800	},
IF{B3600}	{	12,	3600,	B3600	},
IF{B2400}	{	11,	2400,	B2400	},
IF{B1800}	{	10,	1800,	B1800	},
IF{B1200}	{	9,	1200,	B1200	},
IF{B900} 	{	9,	900,	B900	},
IF{B600} 	{	8,	600,	B600	},
IF{B300} 	{	7,	300, 	B300	},
IF{B200} 	{	6,	200, 	B200	},
IF{B150} 	{	5,	150,	B150	},
IF{B134} 	{	4,	134,	B134	},
IF{B110} 	{	3,	110,	B110	},
IF{B75}  	{	2,	75,	B75	},
IF{B50}  	{	1,	50,	B50	},
IF{B0}   	{	0,	0,	B0	},
		{	-1,	-1,	-1	}
};

/*
 * baud may either be a bits-per-second value or a symbolic
 * value as returned by cfget?speed()
 */
struct baud_values *
lookup_baud(baud)
int baud;
{
  struct baud_values *p;

  for (p = btable; p->idx >= 0; p++)
    if (baud == p->bps || baud == p->sym)
      return p;
  return NULL;
}

/*
 * change the baud rate in a mode structure.
 * ibaud and obaud are given in bit/second, or at your option as
 * termio B... symbols as defined in e.g. suns sys/ttydev.h
 * -1 means don't change.
 */
int
SetBaud(m, ibaud, obaud)
struct mode *m;
int ibaud, obaud;
{
  struct baud_values *ip, *op;

  if ((!(ip = lookup_baud(ibaud)) && ibaud != -1) ||
      (!(op = lookup_baud(obaud)) && obaud != -1))
    return -1;

#ifdef POSIX
  if (ip) cfsetispeed(&m->tio, ip->sym);
  if (op) cfsetospeed(&m->tio, op->sym);
#else /* POSIX */
# ifdef TERMIO
  if (ip)
    {
#  ifdef IBSHIFT
      m->tio.c_cflag &= ~(CBAUD << IBSHIFT);
      m->tio.c_cflag |= (ip->sym & CBAUD) << IBSHIFT;
#  else /* IBSHIFT */
      if (ibaud != obaud)
        return -1;
#  endif /* IBSHIFT */
    }
  if (op)
    {
      m->tio.c_cflag &= ~CBAUD;
      m->tio.c_cflag |= op->sym & CBAUD;
    }
# else /* TERMIO */
  if (ip) m->m_ttyb.sg_ispeed = ip->idx;
  if (op) m->m_ttyb.sg_ospeed = op->idx;
# endif /* TERMIO */
#endif /* POSIX */
  return 0;
}


int
CheckTtyname (tty)
char *tty;
{
  struct stat st;
  char realbuf[PATH_MAX];
  const char *real;
  int rc;

  real = realpath(tty, realbuf);
  if (!real)
    return -1;
  realbuf[sizeof(realbuf)-1]='\0';

  if (lstat(real, &st) || !S_ISCHR(st.st_mode) ||
    (st.st_nlink > 1 && strncmp(real, "/dev", 4)))
    rc = -1;
  else
    rc = 0;

  return rc;
}

/*
 *  Write out the mode struct in a readable form
 */

#ifdef DEBUG
void
DebugTTY(m)
struct mode *m;
{
  int i;

#ifdef POSIX
  debug("struct termios tio:\n");
  debug1("c_iflag = %#x\n", (unsigned int)m->tio.c_iflag);
  debug1("c_oflag = %#x\n", (unsigned int)m->tio.c_oflag);
  debug1("c_cflag = %#x\n", (unsigned int)m->tio.c_cflag);
  debug1("c_lflag = %#x\n", (unsigned int)m->tio.c_lflag);
  debug1("cfgetospeed() = %d\n", (int)cfgetospeed(&m->tio));
  debug1("cfgetispeed() = %d\n", (int)cfgetispeed(&m->tio));
  for (i = 0; i < sizeof(m->tio.c_cc)/sizeof(*m->tio.c_cc); i++)
    {
      debug2("c_cc[%d] = %#x\n", i, m->tio.c_cc[i]);
    }
# ifdef HPUX_LTCHARS_HACK
  debug1("suspc     = %#02x\n", m->m_ltchars.t_suspc);
  debug1("dsuspc    = %#02x\n", m->m_ltchars.t_dsuspc);
  debug1("rprntc    = %#02x\n", m->m_ltchars.t_rprntc);
  debug1("flushc    = %#02x\n", m->m_ltchars.t_flushc);
  debug1("werasc    = %#02x\n", m->m_ltchars.t_werasc);
  debug1("lnextc    = %#02x\n", m->m_ltchars.t_lnextc);
# endif /* HPUX_LTCHARS_HACK */
#else /* POSIX */
# ifdef TERMIO
  debug("struct termio tio:\n");
  debug1("c_iflag = %04o\n", m->tio.c_iflag);
  debug1("c_oflag = %04o\n", m->tio.c_oflag);
  debug1("c_cflag = %04o\n", m->tio.c_cflag);
  debug1("c_lflag = %04o\n", m->tio.c_lflag);
  for (i = 0; i < sizeof(m->tio.c_cc)/sizeof(*m->tio.c_cc); i++)
    {
      debug2("c_cc[%d] = %04o\n", i, m->tio.c_cc[i]);
    }
# else /* TERMIO */
  debug1("sg_ispeed = %d\n",    m->m_ttyb.sg_ispeed);
  debug1("sg_ospeed = %d\n",    m->m_ttyb.sg_ospeed);
  debug1("sg_erase  = %#02x\n", m->m_ttyb.sg_erase);
  debug1("sg_kill   = %#02x\n", m->m_ttyb.sg_kill);
  debug1("sg_flags  = %#04x\n", (unsigned short)m->m_ttyb.sg_flags);
  debug1("intrc     = %#02x\n", m->m_tchars.t_intrc);
  debug1("quitc     = %#02x\n", m->m_tchars.t_quitc);
  debug1("startc    = %#02x\n", m->m_tchars.t_startc);
  debug1("stopc     = %#02x\n", m->m_tchars.t_stopc);
  debug1("eofc      = %#02x\n", m->m_tchars.t_eofc);
  debug1("brkc      = %#02x\n", m->m_tchars.t_brkc);
  debug1("suspc     = %#02x\n", m->m_ltchars.t_suspc);
  debug1("dsuspc    = %#02x\n", m->m_ltchars.t_dsuspc);
  debug1("rprntc    = %#02x\n", m->m_ltchars.t_rprntc);
  debug1("flushc    = %#02x\n", m->m_ltchars.t_flushc);
  debug1("werasc    = %#02x\n", m->m_ltchars.t_werasc);
  debug1("lnextc    = %#02x\n", m->m_ltchars.t_lnextc);
  debug1("ldisc     = %d\n",    m->m_ldisc);
  debug1("lmode     = %#x\n",   m->m_lmode);
# endif /* TERMIO */
#endif /* POSIX */
}
#endif /* DEBUG */
