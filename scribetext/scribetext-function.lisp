

(in-package :cl-documenter)
(defparameter *token* 0)
(defparameter *device* nil)

(defparameter *scribetext-functions*
  '(delete
    symbol
    strip
    device
    imbed
    footnote
    end
    caps
    tilde
    nop
    include
    chapter
    define
    newpage
    stupidstrip
    verbatim
    itemize
    blankspace
    label
    ref
    pageref
    tsymbol
    define))


(defun st-unique-id ()
  (incf *token*))

(defun st-device (command to-find)
  (when  (string= *device* "postscript")
    (format t "* Output device ~s was specified in input file.~%"
	    *device*)
    (format t "Please note that printer selections must occur ~s~%"
	    "from within the specific")
    (format t "Andrew application.~%")))
